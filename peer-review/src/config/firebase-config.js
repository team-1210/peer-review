import {initializeApp} from 'firebase/app';
import {getAuth} from 'firebase/auth';
import {getDatabase} from 'firebase/database';
import {getStorage} from 'firebase/storage';


const firebaseConfig = {
  apiKey: 'AIzaSyAAB1n-fZRWhl4b2R7T12Vhqci1UOGlqXw',
  authDomain: 'peer-review-ca86e.firebaseapp.com',
  databaseURL: 'https://peer-review-ca86e-default-rtdb.europe-west1.firebasedatabase.app',
  projectId: 'peer-review-ca86e',
  storageBucket: 'peer-review-ca86e.appspot.com',
  messagingSenderId: '286498317059',
  appId: '1:286498317059:web:eaf4076d541596ced3bf36',
  databaseURL: 'https://peer-review-ca86e-default-rtdb.europe-west1.firebasedatabase.app/',
};


export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const db = getDatabase(app);
export const storage = getStorage(app);

