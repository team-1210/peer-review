import './App.css';
import '@progress/kendo-theme-default/dist/all.css';
import React, {useEffect, useState} from 'react';
import {BrowserRouter, Route, Routes, Navigate} from 'react-router-dom';
import {Register} from './views/Register/Register';
import {Login} from './views/Login/Login';
import {Home} from './views/Home/Home';
import {AppContext} from './providers/AppContext';
import {Contacts} from './components/Contacts/Contacts';
import {About} from './components/About/About';
import {getUserData} from './services/users.services';
import {useAuthState} from 'react-firebase-hooks/auth';
import {auth} from './config/firebase-config';
import {NotFound} from './components/NotFound/NotFound';
import {Settings} from './views/Settings/Settings';
import EditInformation from
  './views/EditAccount/EditInformation/EditInformation';
import UploadPicture from './views/EditAccount/UploadPicture/UploadPicture';
import {AddNewTeam} from './views/AddTeam/AddTeam';
import {AddTask} from './views/AddTask/AddTask';
import {NavBar} from './components/NavBar/NavBar';
import {Authenticated} from './components/hoc/Authenticated';
import {EditPassword} from './views/EditAccount/EditPassword/EditPassword';
import {BoardTask} from './views/TaskBoard/TaskBoard';
import {Projects} from './views/Projects/Projects';
import {AdminPanel} from './views/Admin Panel/AdminPanel';
import {MyTeams} from './components/MyTeams/MyTeams';
import {TasksView} from './components/Tasks/Tasks';
import {SingleTaskView} from './views/SingleTaskView/SingleTaskView';
import {EditTeam} from './views/EditTeamInformation/EditTeamInformation';
import {TasksOverview} from './components/Tasks/TasksOverview';
import Notifications from './views/Notifications/Notifications';

const ADMIN_USER = [3];
const MIXED_USERS = [0, 1, 2, 3, 4];


const App = () => {
  const [appState, setAppState] = useState({
    user: null,
    userData: null,
  });


  const [user, loading] = useAuthState(auth);

  useEffect(() => {
    if (user === null) return;

    getUserData(user.uid)
        .then((snapshot) => {
          if (!snapshot.exists()) {
            throw new Error('Something went wrong!');
          }

          setAppState({
            user,
            userData: snapshot.val()[Object.keys(snapshot.val())[0]],
          });
        })
        .catch((e) => alert(e.message));
  }, [user]);


  return (
    <BrowserRouter>
      <AppContext.Provider value={{...appState, setContext: setAppState}}>
        <div className='App'>
          <NavBar />
          <div className='Outlet'>
            <Routes>
              <Route index element={<Navigate replace to="home" />} />
              <Route path='home' element={<Home />} />
              <Route path='about-us' element={<About />} />
              <Route path='register' element={<Register />} />
              <Route path='login' element={<Login />} />
              <Route path='board-task' element={<Authenticated allowedUsers={MIXED_USERS} loading={loading}><BoardTask /></Authenticated>} />
              <Route path='add-new-task' element={<Authenticated allowedUsers={MIXED_USERS} loading={loading}><AddTask /></Authenticated>} />
              <Route path='contact-us' element={<Contacts />} />
              <Route path="settings" element={<Authenticated allowedUsers={MIXED_USERS} loading={loading}><Settings /></Authenticated>} />
              <Route path="edit-account"
                element={<Authenticated allowedUsers={MIXED_USERS} loading={loading}><EditInformation /></Authenticated>} />
              <Route path="edit-password" element={<EditPassword />} />
              <Route path="upload-picture" element={<Authenticated allowedUsers={MIXED_USERS} loading={loading}><UploadPicture /></Authenticated>} />
              <Route path="admin-panel" element={<Authenticated allowedUsers={ADMIN_USER} loading={loading}><AdminPanel /></Authenticated>} />
              <Route path="admin-panel/:teamName" element={<Authenticated allowedUsers={MIXED_USERS} loading={loading}><EditTeam /></Authenticated>} />
              <Route path="admin-panel/:task/view" element={<Authenticated allowedUsers={MIXED_USERS} loading={loading}><TasksView /></Authenticated>} />
              <Route path="project-lab" element={<Authenticated allowedUsers={MIXED_USERS} loading={loading}><MyTeams /></Authenticated>} />
              <Route path="project-lab/:teamName" element={<Authenticated allowedUsers={MIXED_USERS} loading={loading}><EditTeam /></Authenticated>} />
              <Route path="project-lab/tasks/overview" element={<Authenticated allowedUsers={MIXED_USERS} loading={loading}><TasksOverview /></Authenticated>} />
              <Route path="pending-teams" element={<Authenticated allowedUsers={MIXED_USERS} loading={loading}><Projects /></Authenticated>} />
              <Route path="project-lab/tasks" element={<Authenticated allowedUsers={MIXED_USERS} loading={loading}><TasksView /></Authenticated>} />
              <Route path="project-lab/tasks/overview/:item" element={<Authenticated allowedUsers={MIXED_USERS} loading={loading}><SingleTaskView /></Authenticated>} />
              <Route path="add-new-team" element={<Authenticated allowedUsers={MIXED_USERS} loading={loading}><AddNewTeam /></Authenticated>} />
              <Route path="*" element={<NotFound />} />
              <Route path="notifications" element={<Authenticated allowedUsers={MIXED_USERS} loading={loading}><Notifications /></Authenticated>} />
            </Routes>
          </div>
          {/* <Footer /> */}
        </div>
      </AppContext.Provider>
    </BrowserRouter>
  );
};

export default App;
