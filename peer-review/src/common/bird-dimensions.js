export const birdDimensions = new Map();

birdDimensions.set('(min-width: 1885px) and (max-width: 2150px)', {multiplierOffset: {vertical: 2.5, horizontal: 3.7}});
birdDimensions.set('(min-width: 1720px) and (max-width: 1885px)', {multiplierOffset: {vertical: 2.5, horizontal: 3.2}});
birdDimensions.set('(min-width: 1555px) and (max-width: 1720px)', {multiplierOffset: {vertical: 2.5, horizontal: 3}});
birdDimensions.set('(min-width: 1380px) and (max-width: 1555px)', {multiplierOffset: {vertical: 2.5, horizontal: 2.7}});
birdDimensions.set('(min-width: 1215px) and (max-width: 1395px)', {multiplierOffset: {vertical: 2.4, horizontal: 2.3}});
birdDimensions.set('(min-width: 1090px) and (max-width: 1230px)', {multiplierOffset: {vertical: 2.4, horizontal: 2.1}});
birdDimensions.set('(min-width: 925px) and (max-width: 1090px)', {multiplierOffset: {vertical: 2.4, horizontal: 1.8}});
birdDimensions.set('(min-width: 750px) and (max-width: 925px)', {multiplierOffset: {vertical: 2.4, horizontal: 1.4}});
birdDimensions.set('(min-width: 570px) and (max-width: 750px)', {multiplierOffset: {vertical: 2.5, horizontal: 1.2}});
birdDimensions.set('(min-width: 400px) and (max-width: 570px)', {multiplierOffset: {vertical: 2.7, horizontal: 0.8}});
birdDimensions.set('(min-width: 250px) and (max-width: 400px)', {multiplierOffset: {vertical: 2.8, horizontal: 0.4}});
