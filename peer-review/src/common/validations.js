/* eslint-disable no-unused-vars */
import React from 'react';
import {FieldWrapper} from '@progress/kendo-react-form';
import {
  Label,
  Error,
} from '@progress/kendo-react-labels';
import {Input} from '@progress/kendo-react-inputs';

const emailRegex = new RegExp(/\S+@\S+\.\S+/);

export const emailValidator = (value) =>
   emailRegex.test(value) ? '' : 'Please enter a valid email.';

export const firstNameValidator = (value) =>
  value && value.trim().length > 3 && value.trim().length < 33 ?
   '' : 'First Name must be between 4 and 32 symbols!';

export const lastNameValidator = (value) =>
  value && value.trim().length > 3 && value.trim().length < 33 ?
   '' : 'Last Name must be between 4 and 32 symbols!';

export const handleValidator = (value) =>
  value && value.trim().length > 1 && value.trim().length < 21 ?
   '' : 'Username must be between 2 and 20 symbols!';

export const phoneNumberValidator = (value) =>
  value && value.trim().length === 10 ?
  '' : 'Phone Number must be 10 symbols!';

export const passwordValidator = (value) =>
  value && value.trim().length > 5 ?
   '' : 'Password should be 6 or more symbols';

export const nameValidator = (value) =>
   value && value.trim().length > 2 && value.trim().length < 31 ?
    '' : 'Name must be between 3 and 30 symbols!';

export const titleValidator = (value) =>
    value && value.trim().length > 9 && value.trim().length < 81 ?
     '' : 'Item title should be between 10 and 80 symbols!';

export const descriptionValidator = (value) =>
     value && value.trim().length > 20 ?
      '' : 'Description must be over 20 symbols!';

export const ValidatedInput = (fieldRenderProps) => {
  const {validationMessage, visited, ...others} = fieldRenderProps;
  return (
    <>
      <div className='field-title'>{fieldRenderProps.name}</div>
      <Input {...others} />
      {visited && validationMessage && <Error>{validationMessage}</Error>}
    </>
  );
};
