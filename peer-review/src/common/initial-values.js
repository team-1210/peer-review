export const allInitialValues = (userData) => {
  return (
    {
      firstName: userData ? userData.firstName : '',
      lastName: userData ? userData.lastName : '',
      handle: userData ? userData.handle : '',
      email: userData ? userData.email : '',
      phoneNumber: userData ? userData.phoneNumber : '',
    }
  );
};
