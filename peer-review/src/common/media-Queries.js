export const mediaQueries = [
  '(min-width: 1885px) and (max-width: 2150px)',
  '(min-width: 1720px) and (max-width: 1885px)',
  '(min-width: 1555px) and (max-width: 1720px)',
  '(min-width: 1380px) and (max-width: 1555px)',
  '(min-width: 1215px) and (max-width: 1395px)',
  '(min-width: 1090px) and (max-width: 1230px)',
  '(min-width: 925px) and (max-width: 1090px)',
  '(min-width: 750px) and (max-width: 925px)',
  '(min-width: 570px) and (max-width: 750px)',
  '(min-width: 400px) and (max-width: 570px)',
  '(min-width: 250px) and (max-width: 400px)',
];
