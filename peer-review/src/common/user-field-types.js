import {
  emailValidator,
  firstNameValidator,
  handleValidator,
  lastNameValidator,
  passwordValidator,
  phoneNumberValidator,
  ValidatedInput,
} from './validations';

export const fields = [{
  name: 'user.firstName',
  component: ValidatedInput,
  label: 'First name',
  validator: firstNameValidator,
},
{
  name: 'user.lastName',
  component: ValidatedInput,
  label: 'Last name',
  validator: lastNameValidator,
},
{
  name: 'user.handle',
  component: ValidatedInput,
  label: 'Username',
  validator: handleValidator,
},
{
  name: 'user.email',
  type: 'email',
  component: ValidatedInput,
  label: 'Email',
  validator: emailValidator,
},
{
  name: 'user.phoneNumber',
  component: ValidatedInput,
  label: 'Phone Number',
  validator: phoneNumberValidator,
},
{
  name: 'password',
  type: 'password',
  component: ValidatedInput,
  label: 'Password',
  validator: passwordValidator,
}];
