import {FormTextArea} from '../components/FormTextArea/FormTextArea';
import {
  descriptionValidator,
  titleValidator,
  ValidatedInput} from './validations';

export const workItemFields = [{
  name: 'title',
  component: ValidatedInput,
  tests: 'Title',
  validator: titleValidator,
},
{
  name: 'comments',
  label: 'Description',
  component: FormTextArea,
  validator: descriptionValidator,
},
];
