export const birdConstants = [{
  title: 'Hello, we are PEER INTERIOR.',
  color: '#292629',
  partsToFill: ['car', 'roof', 'outer-window', 'window-sm'],
}, {
  title: 'This is the interior design "playground" for your business!',
  color: '#E17277',
  partsToFill: ['inner-roof', 'inner-house'],
}, {
  title: 'We welcome you design enthusiasts to assist with your day-to-day challenges.',
  color: '#95FFFF',
  partsToFill: ['circle-upper-window', 'rectangle-window',
    'triangle-upper-window', 'triangle-down-window', 'circle-down-window'],
}, {
  title: 'This all started from a simple college project and we are so happy to be here with you today.',
  color: '#BF515A',
  partsToFill: ['bottom-left', 'bottom-right', 'line-under-roof'],
}, {
  title: 'We hope you enjoy our platform and we complement your organization!',
  color: '#944851',
  partsToFill: ['door'],
},
];
