import React from 'react';
import PropTypes from 'prop-types';

export const SubmitButton = ({text, formProps}) => {
  SubmitButton.propTypes = {
    text: PropTypes.string,
    formProps: PropTypes.object,
  };

  return (
    <button
      type='submit'
      className='k-button k-button-md k-rounded-md
      k-button-solid k-button-solid-base'
      disabled={!formProps.allowSubmit}
    >
      {text}
    </button>
  );
};
