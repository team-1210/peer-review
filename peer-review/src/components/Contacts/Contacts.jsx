import './Contacts.css';
import React from 'react';
import radina from '../../../src/img/81876732_1310870262446630_3318016459845664768_n.jpg';
import stanislav from '../../../src/img/stanislav.JPG';

export const Contacts = () => {
  return (
    <div className="p-5  text-white text-center">
      <h1 className="headerrrr">Hello, we are Radina and Stan!</h1>
      <div className='contacts'>
        <div className='radina'>
          <img className='contacts-img' src={radina} style={{width: '420px', height: '560px'}} alt="Radina" />
          <h2 className="description">Radina Michkyurova</h2>
          <h5 className="description">GitLab: <a href="https://gitlab.com/radinna">https://gitlab.com/radinna</a></h5>
          <h5 className="description">Email: <a href="radinamichkyurova@gmail.com">radinamichkyurova@gmail.com</a></h5>
          <h5 className="description">Telephone: +359 885 167 089</h5>
        </div>
        <div className='stanislav'>
          <img className='contacts-img'src={stanislav} style={{width: '330px', height: '560px'}} alt="Stanislav" />
          <h2 className="description">Stanislav Ivanov</h2>
          <h5 className="description">GitLab: <a href="https://gitlab.com/standimiv">https://gitlab.com/standimiv</a></h5>
          <h5 className="description">Email: <a href="standimiv@yahoo.com">standimiv@yahoo.com</a></h5>
          <h5 className="description">Telephone: +359 888 141 004</h5>
        </div>
      </div>
    </div>
  );
};
