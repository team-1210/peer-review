import React, {useContext, useEffect, useState, useId} from 'react';
import {getAllTeams} from '../../services/teams.services';
import {AppContext} from '../../providers/AppContext';
import {useNavigate} from 'react-router';
import PropTypes from 'prop-types';

export const TasksView = ({taskType}) => {
  TasksView.propTypes = {
    taskType: PropTypes.string,
  };

  const id = useId();
  const teamName = localStorage.getItem('team');
  const navigate = useNavigate();
  const {userData: {handle}} = useContext(AppContext);
  const [teamTasks, setTeamTasks] = useState([]);
  const [myGridData, setMyGridData] = useState([]);


  useEffect(() => {
    getAllTeams()
        .then((teams) => {
          const teamsByTeamName = teams.filter(
              (team) => team.teamName === teamName,
          );
          const myTasks = teamsByTeamName.map((team) => team.workItems);
          const myTasksWithoutIds = myTasks.map((team) => Object.values(team || {}))[0];
          setTeamTasks(myTasksWithoutIds);
          setMyGridData(taskType && taskType !== 'all' && myTasksWithoutIds ? myTasksWithoutIds.filter((task) => task[taskType] === handle) : myTasksWithoutIds);
        })
        .catch((e) => console.log(e));
  }, []);

  const setLocalStorage = (task, taskName) => {
    localStorage.setItem(task, taskName);
  };

  const handleSearch = (searchTerm) => {
    const data = taskType && taskType !== 'all' ? teamTasks.filter((task) => task[taskType] === handle) : teamTasks;
    setMyGridData(
        data.filter((task) => {
          return (
            task.title.toLowerCase().includes(searchTerm.toLowerCase())
          );
        }),
    );
  };


  return (
    <div>
      <h1 className='main-task-title'>{teamName}</h1>
      {myGridData?.length > 0 ?
      <>
        <div className='datatableTitle'>
          <span className="k-spacer"></span>
          <span className="k-searchbox k-input k-input-md k-rounded-md k-input-solid k-grid-search">
            <span className="k-input-icon k-icon k-i-search"></span>
            <input autoComplete='off' placeholder="Search by team name..." title="Search..." onInput={(e) => handleSearch(e.target.value)} className="k-input-inner"/></span>
          <button className='k-button k-button-md k-rounded-md k-button-solid k-button-solid-base' onClick={() => navigate('/add-new-task')}>Add Task</button>
        </div>
        <div className="grid-container">
          {myGridData?.map((task) => {
            return <div key={id} style={{height: '400px'}}>
              <ul>
                <div className="booking-card" style={{backgroundImage: `url(${task.attachmentUrl})`}}>
                  <div className="book-container">
                    <div className="content"></div>
                  </div>
                  <div className="informations-container">
                    <h2 className="title">{task.title}</h2>
                    <p className="price"><svg className="icon" style={{width: '24px', height: '24px'}} viewBox="0 0 24 24">
                      <path fill="currentColor"
                        d="M3,6H21V18H3V6M12,9A3,3 0 0,1 15,12A3,3 0 0,1 12,15A3,3 0 0,1 9,12A3,3 0 0,1 12,9M7,8A2,2 0 0,1 5,10V14A2,2 0 0,1 7,16H17A2,2 0 0,1 19,14V10A2,2 0 0,1 17,8H7Z" />
                    </svg>
                  Status: {task.itemStatus}
                    </p>
                    <div className="more-information">
                      <div className="info-and-date-container">
                        <div className="box info">
                          <svg className="icon" style={{width: '24px', height: '24px'}} viewBox="0 0 24 24">
                            <path fill="currentColor"
                              d="M11,9H13V7H11M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,
                            4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M11,17H13V11H11V17Z" />
                          </svg>
                          <p className="text-box">{task.reviewer !== handle ? `Reviewer ${task.reviewer}` : `Creator: ${task.handle}`}</p>
                        </div>
                        <div className="box date">
                          <svg className="icon" style={{width: '24px', height: '24px'}} viewBox="0 0 24 24">
                            <path fill="currentColor" d="M19,19H5V8H19M16,1V3H8V1H6V3H5C3.89,3 3,3.89 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V5C21,3.89 20.1,3 19,3H18V1M17,12H12V17H17V12Z" />
                          </svg>
                          <p className="text-box"
                            onClick={() => {
                              setLocalStorage('task', task.title);
                              navigate(task.title);
                            }}>View Task Details</p>
                        </div>
                      </div>
                      <p className="disclaimer"></p>
                    </div>
                  </div>
                </div>
              </ul>
            </div>;
          })}
        </div>
      </> :
      <div className='text-container'>
        <div>No tasks at this category yet!</div>
        <div>
          <button className='k-button k-button-md k-rounded-md k-button-solid k-button-solid-base' onClick={() => navigate('/add-new-task')}>Add Task</button>
        </div>
      </div>
      }
    </div>
  );
};
