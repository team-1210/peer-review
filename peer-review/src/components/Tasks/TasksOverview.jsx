import React from 'react';
import {TabStrip, TabStripTab} from '@progress/kendo-react-layout';
import {TasksView} from './Tasks';


export const TasksOverview = () => {
  const [selected, setSelected] = React.useState(1);

  const handleSelect = (e) => {
    setSelected(e.selected);
  };

  return (
    <TabStrip selected={selected} onSelect={handleSelect}>
      <TabStripTab title="Created Tasks" >
        <div>
          <TasksView taskType={'handle'}/>
        </div>
      </TabStripTab>
      <TabStripTab title="Reviewing Tasks">
        <div>
          <TasksView taskType={'reviewer'}/>
        </div>
      </TabStripTab>
      <TabStripTab title="All Team Tasks">
        <div>
          <TasksView taskType={'all'}/>
        </div>
      </TabStripTab>
    </TabStrip>
  );
};
