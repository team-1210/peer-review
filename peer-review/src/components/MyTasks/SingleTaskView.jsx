import React, {useContext, useState} from 'react';
import {Button} from '@progress/kendo-react-buttons';
import {updateWorkItemStatusById} from '../../services/workItems.service';
import {AppContext} from '../../providers/AppContext';
import {Comments} from '../Comments/Comments';
import PropTypes from 'prop-types';


export const SingleTaskView = ({id, owner, description, itemStatus,
  reviewer, created, teamName, title, buttonsText, teamId}) => {
  SingleTaskView.propTypes = {
    id: PropTypes.string,
    owner: PropTypes.string,
    description: PropTypes.string,
    itemStatus: PropTypes.string,
    reviewer: PropTypes.string,
    created: PropTypes.string,
    teamName: PropTypes.string,
    title: PropTypes.string,
    buttonsText: PropTypes.array,
    teamId: PropTypes.string,
  };

  const [status, setStatus] = useState([]);

  const {userData: {handle}} = useContext(AppContext);

  const updateTeamStatus = (id, status) => {
    return updateWorkItemStatusById(id, status, teamId)
        .then((r) => setStatus(status))
        .catch((e) => console.error(e));
  };

  const handleComment = (handle, id) => {
    <Comments handle={handle} id={id} status={status}/>;
  };

  return (
    <>
      <div className='organizational-containers'>
      Team Name:<br/>{teamName}<br/><br/>
      Task Title:<br/>{title}<br/><br/>
      Task Reviewer:<br/>{reviewer}<br/><br/>
      Task Description:<br/>{description}<br/><br/>
      Task status:<br/>{itemStatus}<br/><br/>
      Team admin/s:<br/>{owner}<br/><br/>
      Created:<br/>{new Date(created).toString().substring(0, 21)}<br/><br/>

        {itemStatus === 'Pending' ?
        <Button fillMode="outline"
          onClick={() => updateTeamStatus(id, 'Under Review')}
        >{buttonsText[0]}</Button> : null
        }

        {itemStatus === 'Under Review' ?
        <Button fillMode="outline"
          onClick={() => handleComment(handle, id)}
        >{buttonsText[1]}</Button> : null
        }

        {itemStatus === 'Under Review' ?
        <Button fillMode="outline"
          onClick={() => (updateTeamStatus(id, 'Accepted'))}
        >{buttonsText[2]}</Button> : null
        }

        {itemStatus === 'Under Review' ?
        <Button fillMode="outline"
          onClick={() => (updateTeamStatus(id, 'Rejected'))}
        >{buttonsText[3]}</Button> : null
        }
      </div>
    </>
  );
};
