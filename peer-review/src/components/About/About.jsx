import './About.css';
import React, {useEffect} from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css';

export const About = () => {
  useEffect(() => {
    AOS.init();
    AOS.refresh();
  }, []);
  return (
    <>
      <div className="containerrrr">
        <h2 className="home-title">We are PEER INTERIOR. </h2>
        <p className="home-title-s">If you have great interior design ideas, we provide you the ultimate platform to expose their spirit with your colleagues! </p>

        <div className="row items-center mb4">
          <div className="col-12 col-sm-6 order-last order-first-lg mb3">
            <h3 className="home-title">Our Mission </h3>
            <p className="home-title-s">We believe that achieving better results in any industry is truly a complex task, requiring a lot of effort and time. We strive to alleviate this
            complexity for the interior design community.

            </p>
          </div>
          <div className="col-12 col-sm-5 order-first order-last-lg tc mb3">
            <img src="https://www.transparentpng.com/thumb/team-work/team-work-png-file-clipart-4.png" data-aos-offset='150' data-aos="fade-up"/>
          </div>
        </div>

        <div className="row items-center mb4">
          <div className="col-12 col-sm-6 mb3">
            <h3 className="home-title">More On Our Goals </h3>
            <p className="home-title-s">Our platform strives at eliminating the burdens of organizing peer review processes across your organization.
            Higher management should focus on long-term goals and not on procedural details. We hope you benefit from PEER INTERIOR and its perks!</p>
          </div>
          <div className="col-12 col-sm-4 mb3">
            <img src="https://www.transparentpng.com/thumb/teacher/1wPiZl-teacher-with-apple-tablet-in-hand-picture-shoulder-turn-technology-informatics-knowledgeable.png"
              data-aos="fade-up" data-aos-offset='150'/>
          </div>
        </div>

        <div className="row items-center mb4">
          <div className="col-12 col-sm-6 order-last order-first-lg mb3">
            <h3 className="home-titleK">Our Story</h3>
            <p className="home-title-s">Radina and Stan met at Telerik Academy where it all started from a simple graduate project.
            They decided to build further upon the ideas around the project and this is how PEER INTERIOR came to prominence.</p>
            <p className="home-title-s">We have been established in the interior design market for 5 years now and build further on an idea that was not left abandoned. </p>
          </div>
          <div className="col-12 col-sm-5 order-first order-last-lg tc mb3">
            <img src="https://www.transparentpng.com/thumb/team-work/team-work-photos-9.png" data-aos-offset='150' data-aos="fade-up"/>
          </div>
        </div>
      </div>
    </>
  );
};
