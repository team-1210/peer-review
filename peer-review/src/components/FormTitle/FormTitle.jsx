import React from 'react';
import PropTypes from 'prop-types';
export const FormTitle = ({title}) => {
  FormTitle.propTypes = {
    title: PropTypes.string,
  };

  return <legend className={'k-form-legend'}>{title}</legend>;
};
