import './NavBar.css';
import React from 'react';
import {NavLink} from 'react-router-dom';
import {useContext} from 'react';
import {AppContext} from '../../providers/AppContext';
import {NotificationBell} from '../NotificationBell/NotificationBell';
import {logoutUser} from '../../services/auth.service';

export const NavBar = () => {
  const {user, userData, setContext} = useContext(AppContext);

  const logout = () => {
    logoutUser()
        .then(() => {
          setContext({user: null, userData: null});
        });
  };


  return (
    <div className='navbar-section'>
      <div className='menu-wrapper'>
        <ul id="menu-menu" className='menu menu-main'>
          <li id="menu-item-206" className='menu-item menu-item-type-post_type menu-item-object-page'>
            <NavLink to='home'><span>Home</span></NavLink>
          </li>
          <li id="menu-item-206" className='menu-item menu-item-type-post_type menu-item-object-page'>
            <NavLink to='about-us'><span>About</span></NavLink>
          </li>
          <li id="menu-item-206" className='menu-item menu-item-type-post_type menu-item-object-page'>
            <NavLink to='contact-us'><span>Contacts</span></NavLink>
          </li>
          {!user ?
        <>
          <li id="menu-item-206" className='menu-item menu-item-type-post_type menu-item-object-page'>
            <NavLink to='register'><span>Register</span></NavLink>
          </li>
          <li id="menu-item-206" className='menu-item menu-item-type-post_type menu-item-object-page'>
            <NavLink to='login'><span>Login</span></NavLink>
          </li>
        </> : null}
          {user ?
        <>
          <li id="menu-item-206" className='menu-item menu-item-type-post_type menu-item-object-page'>
            <NavLink to='pending-teams'><span>Pending</span></NavLink>
          </li>
          <li id="menu-item-206" className='menu-item menu-item-type-post_type menu-item-object-page'>
            <NavLink to='project-lab'><span>Project Lab</span></NavLink>
          </li>
          <li id="menu-item-206" className='menu-item menu-item-type-post_type menu-item-object-page'>
            <NavLink to='board-task'><span>My board</span></NavLink>
          </li>
          <li id="menu-item-206" className='menu-item menu-item-type-post_type menu-item-object-page'>
            <NavLink to='settings'><span>Setting</span></NavLink>
          </li>
        </> : null}
          {user && userData.role === '3' ?
        <li id="menu-item-206" className='menu-item menu-item-type-post_type menu-item-object-page'>
          <NavLink to='admin-panel'><span>Admin Panel</span></NavLink>
        </li> : null}
          {user ?
          <li id="menu-item-206" className='bell-container menu-item menu-item-type-post_type menu-item-object-page'>
            <NavLink to='notifications'><NotificationBell /></NavLink>
          </li>: null}
          {user ? <button type="button" onClick={logout} className="btn btn-default btn-sm">
             Log out <span className="k-icon .k-i-logout"></span>
          </button> : null}
        </ul>
      </div>
    </div>
  );
};
