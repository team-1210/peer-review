import React from 'react';
import PropTypes from 'prop-types';

export const FormRenderValidation = ({formRenderProps}) => {
  FormRenderValidation.propTypes = {
    formRenderProps: PropTypes.object,
  };

  return (
    formRenderProps.visited &&
    formRenderProps.errors &&
    formRenderProps.errors.VALIDATION_SUMMARY && (
      <div className={'k-messagebox k-messagebox-error'}>
        {formRenderProps.errors.VALIDATION_SUMMARY}
      </div>
    )
  );
};
