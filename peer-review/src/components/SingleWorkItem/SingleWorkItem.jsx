import './SingleWorkItem.css';
import React, {useContext} from 'react';
import {AppContext} from '../../providers/AppContext';
import PropTypes from 'prop-types';


const Item = ({deleteItem, item}) => {
  const {userData: {handle, role}} = useContext(AppContext);

  Item.propTypes = {
    deleteItem: PropTypes.func,
    item: PropTypes.object,
  };


  const canDelete = () => item.creator === handle || role === 3;

  return (
    <div className='organizational-containers item'>
      <div className='Item-Header'>{`Creator: ${item.creator}`}</div><br/>
      <div className='Item-Header'>{`Title: ${item.title}`}</div><br />
      <div className='Item-Header'>{`Description:
${item.description}`}</div><br />
      <div className='Item-Header'>{`Creation Time:
${item.createdOn.toUTCString()}`}</div><br />
      <div className='Item-Header'>{`Status: ${item.itemStatus}`}</div>
      <div className='Item-Header'>{`Reviewer: ${item.reviewer}`}</div>

      <div className='Item-Meta'>
        {canDelete() && <button onClick={() =>
          deleteItem(item.id, handle)}>Delete</button>}
        <div className='Item-Header'>{item.attachment ? <a href={item.attachment}>Attachment</a> : null}</div>
      </div>
    </div>
  );
};

export default Item;
