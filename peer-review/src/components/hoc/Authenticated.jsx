import React, {useContext} from 'react';
import {Navigate, useLocation} from 'react-router-dom';
import {AppContext} from '../../providers/AppContext';
import PropTypes from 'prop-types';

/**
 *
 * @param {*} param0
 * @return {*}
 */
export const Authenticated = ({children, loading, allowedUsers}) => {
  Authenticated.propTypes = {
    children: PropTypes.object,
    loading: PropTypes.boolean,
    allowedUsers: PropTypes.array,
  };

  const {user, userData} = useContext(AppContext);
  const location = useLocation();
  const keyRole = Number(userData?.role);

  if (loading) {
    return <h1>loading...</h1>;
  }

  if (!user) {
    return <Navigate to='/home' state={{from: location}} />;
  }

  const userHasRequiredRole = allowedUsers.includes(keyRole);

  if (user && !userHasRequiredRole) {
    return <Navigate to='/home' state={{from: location}} />;
  }
  return children;
};
