import './NotificationBell.css';
import {onValue, ref} from 'firebase/database';
import React, {useContext, useEffect, useState} from 'react';
import {db} from '../../config/firebase-config';
import NotificationBellImg from '../../img/notificationBell.svg';
import {AppContext} from '../../providers/AppContext';
import {updateUserBadges} from '../../services/users.services';

export const NotificationBell = () => {
  const {userData, user, setContext} = useContext(AppContext);
  const [notifications, setNotifications] = useState([]);
  const [diffNotifications, setDiffNotifications] = useState(0);


  useEffect(() => {
    let sub = true;

    const starCountRef = ref(db, 'notifications/' + userData?.uid);
    onValue(starCountRef, (snapshot) => {
      if (snapshot.val() === null) {
        return setNotifications([]);
      }

      if (sub && snapshot.val() !== null) {
        const entriesNotification = Object.entries(snapshot.val());
        const notificationData = entriesNotification.map((not) => ({notificationId: not[0], ...not[1]}),
        );

        setNotifications(notificationData);
        setDiffNotifications(notificationData.length - userData.badges);
      }
      return () => {
        sub = false;
      };
    });
  }, [userData?.uid, userData.badges]);

  const handleClick = () => {
    updateUserBadges(userData?.handle, notifications.length);
    const newData = {...userData, badges: notifications.length};
    setContext({
      user,
      userData: newData,
    });
  };

  return (
    <div className='notification-container'>
      {diffNotifications > 0 ? <img src={NotificationBellImg} className='bellActive' onClick={handleClick}
        alt='bellActive'></img> : <img src={NotificationBellImg} className='bellNotActive' onClick={handleClick} alt='bellNotActive'></img>}
      <div className='notification-counter'>{diffNotifications < 0 ? 0 : diffNotifications}</div>
    </div>
  );
};
