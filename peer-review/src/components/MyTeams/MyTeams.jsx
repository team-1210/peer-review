import React, {useContext, useEffect, useState} from 'react';
import './MyTeams.css';
import {getUserTeams} from '../../services/users.services';
import {useNavigate} from 'react-router';
import {AppContext} from '../../providers/AppContext';

/**
 *
 * @return {*}
 */
export const MyTeams = () => {
  const [myTeams, setMyTeams] = useState([]);
  const {userData: {handle}} = useContext(AppContext);
  const [gridData, setGridData] = useState([]);
  const navigate = useNavigate();

  const setLocalStorage = (team, teamName) => {
    localStorage.setItem(team, teamName);
  };


  useEffect(() => {
    getUserTeams(handle)
        .then((teams) => {
          const myTeams = teams.filter((invitationId) => invitationId.status === 'Accepted');
          setMyTeams(myTeams);
          setGridData(myTeams);
        })
        .catch(console.error);
  }, []);

  const viewPendingTeams = () => {
    navigate('/pending-teams');
  };

  const handleSearch = (searchTerm) => {
    setGridData(
        myTeams.filter((team) => {
          return (
            team.teamName.toLowerCase().includes(searchTerm.toLowerCase())
          );
        }),
    );
  };

  return (
    <>
      <h1 className='main-title'>My Teams</h1>
      {gridData.length > 0 ?
      <>
        <div className='datatableTitle'>
          <span className="k-spacer"></span>
          <span className="k-searchbox k-input k-input-md k-rounded-md k-input-solid k-grid-search">
            <span className="k-input-icon k-icon k-i-search"></span>
            <input autoComplete='off' placeholder="Search by team name..." title="Search..." onInput={(e) => handleSearch(e.target.value)} className="k-input-inner"/></span>
          <button className='k-button k-button-md k-rounded-md k-button-solid k-button-solid-base k-button-add-new' onClick={() => navigate('/add-new-team')}>Add New</button>
        </div>
        <div className='teams-container'>
          <div>
            <div className='grid-container'>
              {gridData.map((r) => {
                return <div key={r.id} style={{height: '400px'}}>
                  <ul>
                    <div className="booking-card team-booking-card" style={{backgroundImage: `url(${r.teamPicture})`}}>
                      <div className="book-container">
                        <div className="content"></div>
                      </div>
                      <div className="informations-container">
                        <h2 className="title">{r.teamName}</h2>
                        <p className="price"><svg className="icon" style={{width: '24px', height: '24px'}} viewBox="0 0 24 24">
                          <path fill="currentColor"
                            d="M3,6H21V18H3V6M12,9A3,3 0 0,1 15,12A3,3 0 0,1 12,15A3,3 0 0,1 9,12A3,3 0 0,1 12,9M7,8A2,2 0 0,1 5,10V14A2,2 0 0,1 7,16H17A2,2 0 0,1 19,14V10A2,2 0 0,1 17,8H7Z" />
                        </svg>Team Members: {r.teamMembers.length}</p>
                        <div className="more-information">
                          <div className="info-and-date-container">
                            <div className="box info">
                              <svg className="icon" style={{width: '24px', height: '24px'}} viewBox="0 0 24 24">
                                <path fill="currentColor"
                                  d="M11,9H13V7H11M12,20C7.59,20 4,16.41 4,12C4,7.59
                                  7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M11,17H13V11H11V17Z" />
                              </svg>
                              <p className="text-box"
                                onClick={() => {
                                  setLocalStorage('team', r.teamName);
                                  setLocalStorage('teamId', r.id);
                                  navigate(r.teamName);
                                }}>Team Details</p>
                            </div>
                            <div className="box date">
                              <svg className="icon" style={{width: '24px', height: '24px'}} viewBox="0 0 24 24">
                                <path fill="currentColor" d="M19,19H5V8H19M16,1V3H8V1H6V3H5C3.89,3 3,3.89 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V5C21,3.89 20.1,3 19,3H18V1M17,12H12V17H17V12Z" />
                              </svg>
                              <p className="text-box"
                                onClick={() => {
                                  setLocalStorage('team', r.teamName);
                                  setLocalStorage('teamId', r.id);
                                  navigate('tasks/overview');
                                }}>Team Tasks</p>
                            </div>
                          </div>
                          <p className="disclaimer"></p>
                        </div>
                      </div>
                    </div>
                  </ul>
                </div>;
              })}
            </div>
          </div>
        </div>
      </> :
      <div className='text-container'>
        <div>You are not included in a team yet!</div>
        <div>Please check your Pending Teams <span className='navigate-link' onClick={viewPendingTeams}>here.</span></div>
        <button className='k-button k-button-md k-rounded-md k-button-solid k-button-solid-base k-button-add-new' onClick={() => navigate('/add-new-team')}>Add New</button>
      </div>}
    </>
  );
};
