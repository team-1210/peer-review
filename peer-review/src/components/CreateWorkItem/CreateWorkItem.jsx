import './CreateWorkItem.css';
import React, {useState, useEffect, useContext} from 'react';
import PropTypes from 'prop-types';
import {Form, Field, FormElement} from '@progress/kendo-react-form';
import {MultiSelect} from '@progress/kendo-react-dropdowns';
import {useNavigate} from 'react-router-dom';
import {getAllUsers, getUserTeams} from '../../services/users.services';
import {KendoNotification} from '../../views/KendoNotification.jsx/KendoNotifications';
import {AppContext} from '../../providers/AppContext';
import {addWorkItem} from '../../services/workItems.service';
import {getTeamById} from '../../services/teams.services';
import {SubmitButton} from '../SubmitButton/SubmitButton';
import {FormRenderValidation} from '../FormRenderValidation/FormRenderValidation';
import {FormTitle} from '../FormTitle/FormTitle';
import {getDownloadURL, ref as storageRef, uploadBytes} from 'firebase/storage';
import {storage} from '../../config/firebase-config';
import {uid as uidFunc} from 'uid';
import {createNotification} from '../../services/notifications.services';
import {titleValidator} from '../../common/validations';
import {FormTextArea} from '../FormTextArea/FormTextArea';
import {ValidatedInput} from '../../common/validations';
import {descriptionValidator} from '../../common/validations';

const CreateWorkItem = () => {
  const {userData: {handle, uid}} = useContext(AppContext);

  const [userTeams, setUserTeams] = useState([]);
  const [reviewers, setReviewers] = useState([]);
  const [teamId, setTeamId] = useState([]);
  const [attachmentUrl, setAttachmentUrl] = useState([]);

  const navigate = useNavigate();

  const itemStatus = 'Pending';

  useEffect(() => {
    getUserTeams(handle)
        .then((teams) => {
          const accepted = teams.filter((team) => team.status === 'Accepted');
          setUserTeams(accepted);
        })

        .catch(console.error);
  }, []);

  const [teams, setTeams] = useState([]);
  const onChangeTeams = (event) => {
    setTeams([...event.value]);
  };

  useEffect(() => {
    const teamId = userTeams.filter((team) => team.teamName === teams[0]);
    setTeamId(teamId[0]?.id);
  }, [teams]);

  useEffect(() => {
    teamId ?
    getTeamById(teamId)
        .then((team) => {
          const availableReviewers = team.teamMembers?.filter((teamMember) => teamMember !== handle);
          setReviewers(availableReviewers);
        }) : setReviewers(null);
  }, [teamId]);

  const [teamMembers, setTeamMembers] = useState([]);

  const onChange = (event) => {
    setTeamMembers([...event.value]);
  };

  const [state, setState] = useState({
    type: '',
    message: '',
  });

  CreateWorkItem.propTypes = {
    onSubmit: PropTypes.func,
  };


  const handleSubmit = (content) => {
    addWorkItem(content.title, content.comments, itemStatus, teamMembers[0], teams[0], handle, teamId, attachmentUrl)
        .then((result) => {
          setState({type: 'success', message: 'Success!'});

          const data = {
            user: handle,
            userId: uid,
            type: 'Task',
            notification: `You have been assigned as a reviewer for a task ${result.title} in team: ${teams[0]}`,
            teamId: teamId,
            date: new Date().toLocaleDateString(),
          };
          getAllUsers().then((users) => {
            const assignedTo = users.filter((user) => user.handle === teamMembers[0])[0];
            createNotification(assignedTo.uid, data);
          });

          setTimeout(function() {
            navigate('/project-lab');
          }, 400);
        })
        .catch((err) => {
          setState({type: 'error', message: 'Item not created!'});
          setTimeout(() => {
            setState({type: ''});
          }, 4000);
        });
  };

  const uploadPicture = (e) => {
    e.preventDefault();

    const file = e.target[0]?.files?.[0];

    if (!file) {
      setState({type: 'error', message: 'Item not created!'});
      setTimeout(() => {
        setState({type: ''});
      }, 4000);
    };

    const fileValue = file?.name.split('.');
    const fileExtension = fileValue[fileValue.length - 1];

    if (fileExtension.toLowerCase() !== 'pdf' && fileExtension.toLowerCase() !== 'word' && fileExtension.toLowerCase() !== 'docx' && fileExtension.toLowerCase() !== 'zip' &&
      fileExtension.toLowerCase() !== 'jpg' && fileExtension.toLowerCase() !== 'png' && fileExtension.toLowerCase() !== 'jfif' && fileExtension.toLowerCase() !== 'jpeg') {
      setState({type: 'error', message: 'Item not created!'});
      setTimeout(() => {
        setState({type: ''});
      }, 4000);
    }

    const attachment = storageRef(storage, `attachments/${teams[0]}/${uidFunc()}`);

    uploadBytes(attachment, file)
        .then((snapshot) => {
          return getDownloadURL(snapshot.ref)
              .then((url) => {
                setAttachmentUrl(url);
                setState({type: 'success', message: 'Success!'});
                setTimeout(() => {
                }, 1000);
              });
        })
        .catch(console.error);
  };

  return (
    <>
      <KendoNotification type={state.type}
        message={state.message}></KendoNotification>
      <Form
        onSubmit={handleSubmit}
        render={(formRenderProps) => (
          <FormElement
            className='form-input-fields'
          >
            <fieldset className={'k-form-fieldset'}>
              <FormTitle title='Create new item:'/>
              <FormRenderValidation formRenderProps = {formRenderProps}/>
              <div>Add Team</div>
              <MultiSelect data={userTeams.map((u) => u.teamName)}
                fillMode={'outline'}
                onChange={onChangeTeams} value={teams} />

              <div className='mb-3'>
                <br />
                <div>Title</div>
                <Field
                  name='title'
                  component= {ValidatedInput}
                  validator={titleValidator}
                />
              </div>
              <div className='mb-3'>
                <Field
                  name='comments'
                  component= {FormTextArea}
                  label='Description'
                  validator={descriptionValidator}
                />
              </div>

              <div>Add reviewers</div>
              <MultiSelect data={reviewers?.map((u) => u)}
                fillMode={'outline'}
                onChange={onChange} value={teamMembers} />
            </fieldset>
            <div className="example-wrapper">
            </div>
            <div className='k-form-buttons'>
              <SubmitButton text={'Create New Item'} formProps={formRenderProps}/>
            </div>
          </FormElement>
        )}
      />
      { teamId ? <form id="uploadForm" onSubmit={uploadPicture}>
        <input className="uploadInput" type="file" name="file"></input>
        <button id="submitFormBtn" className='k-button k-button-md k-rounded-md k-button-solid k-button-solid-base' type="submit">Attach</button>
      </form> : null}
    </>
  );
};

export default CreateWorkItem;
