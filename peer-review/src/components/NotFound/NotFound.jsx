import React from 'react';
import './NotFound.css';
import picNotFound from '../../img/picNotFound.png';

/**
 * @return {string}
 */
export const NotFound = () => {
  return (<div>
    <h1 className='error404'>{`Oops... no interiors here :)`}</h1>
    <img className='picNotFound' src={picNotFound} alt='picNotFound' />
  </div>
  );
};
