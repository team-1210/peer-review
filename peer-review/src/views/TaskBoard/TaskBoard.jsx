import React, {useEffect, useState, useContext} from 'react';
import {TaskBoard, TaskBoardToolbar} from '@progress/kendo-react-taskboard';
import {Input} from '@progress/kendo-react-inputs';
import {filterBy} from '@progress/kendo-data-query';
import {Column} from './TaskBoardColumn';
import {Card} from './TaskBoardCard';
import {createTaskBoard, editTaskBoards, getAllColumns, getTaskBoardsByTeam} from '../../services/board.service';
import {MultiSelect} from '@progress/kendo-react-dropdowns';
import {AppContext} from '../../providers/AppContext';
import {getAllWorkItems} from '../../services/workItems.service';
import './TaskBoard.css';

const priorities = [
  {
    priority: 'Low Priority',
    color: 'green',
    team: '',
  },
  {
    priority: 'High Priority',
    color: 'blue',
    team: '',
  },
  {
    priority: 'Urgent',
    color: 'orange',
    team: '',
  },
];

export const BoardTask = () => {
  const [filter, setFilter] = useState('');
  const [taskData, setTaskData] = useState([]);
  const [columnsData, setColumnsData] = useState([]);
  const [userTeams, setUserTeams] = useState([]);
  const {userData: {handle}} = useContext(AppContext);
  const [team, setTeam] = React.useState([]);

  useEffect(() => {
    getAllWorkItems()
        .then((workItems) => {
          const accepted = workItems.filter((workItem) => workItem.reviewer === handle || workItem.creator === handle);
          setUserTeams(accepted);
        })

        .catch(console.error);
  }, []);

  useEffect(() => {
    getTaskBoardsByTeam(team[0])
        .then((cards) => {
          const tasks = cards.map((c) => ({
            id: c.id,
            title: c.title,
            description: c.description,
            status: c.status,
            priority: c.priority,
          }));
          setTaskData(tasks);
        });
  }, [team]);

  useEffect(() => {
    getAllColumns()
        .then((columns) => {
          setColumnsData(columns);
        });
  }, []);

  const onSearchChange = React.useCallback((event) => {
    setFilter(event.value);
  }, []);

  // multiselector 1
  const onChangeTeams = (event) => {
    setTeam([...event.value]);
  };
    // multiselector
  const resultTasks = React.useMemo(() => {
    const filterDesc = {
      logic: 'and',
      filters: [
        {
          field: 'title',
          operator: 'contains',
          value: filter,
        },
      ],
    };
    return filter ? filterBy(taskData, filterDesc) : taskData;
  }, [filter, taskData]);


  const onChangeHandler = (event) => {
    if (event.type === 'column') {
      setColumnsData(event.data);
    } else {
      if (event.item && event.item.id === undefined) {
        event.item.id = Date.now();
        const item = event.item;
        createTaskBoard(team[0], item.id, item.title, item.description, item.status, item.priority, event.type);
      }
      editTaskBoards(team[0], event.item.id, event.item.title, event.item.description, event.item.status, event.item.priority, event.type);
      setTaskData(event.data);
    }
  };

  return (
    <>
      <TaskBoard
        columnData={columnsData}
        taskData={resultTasks}
        priorities={priorities}
        onChange={onChangeHandler}
        column={Column}
        card={Card}
        team = {team[0]}
        style={{
          height: '700px',
        }}
        tabIndex={0}
      >
        <TaskBoardToolbar>
          <MultiSelect data={userTeams.map((u) => u.title)} id='taskboard-multiselect'
            onChange={onChangeTeams} value={team} placeholder='Please select a Task first:' />
          <span className='k-spacer' />

          <Input
            placeholder='Search...'
            onChange={onSearchChange}
            value={filter}
          />
        </TaskBoardToolbar>
      </TaskBoard>
    </>
  );
};
