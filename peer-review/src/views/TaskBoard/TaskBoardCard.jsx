import * as React from 'react';
import {
  TaskBoardCard,
  TaskBoardCardHeader,
} from '@progress/kendo-react-taskboard';

const CardHeaderComponent = (props) => {
  return <TaskBoardCardHeader {...props} />;
};

export const Card = (props) => {
  return (
    <TaskBoardCard
      {...props}
      cardHeader={CardHeaderComponent}
    />
  );
};
