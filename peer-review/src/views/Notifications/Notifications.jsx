import './Notifications.css';
import {onValue, ref} from 'firebase/database';
import React, {useContext, useEffect, useState} from 'react';
import {db} from '../../config/firebase-config';
import {AppContext} from '../../providers/AppContext';
import {deleteNotification, getAllNotificationByUserId} from '../../services/notifications.services';
import {updateUserBadges} from '../../services/users.services';


const Notifications = () => {
  const {userData, user, setContext} = useContext(AppContext);
  const [notifications, setNotifications] = useState([]);

  useEffect(() => {
    let sub = true;

    const starCountRef = ref(db, 'notifications/' + userData?.uid);
    onValue(starCountRef, (snapshot) => {
      if (snapshot.val() === null) {
        return setNotifications([]);
      }

      if (sub && snapshot.val() !== null) {
        const entriesNotification = Object.entries(snapshot.val());
        const notificationData = entriesNotification.map((not) => ({notificationId: not[0], ...not[1]}),
        );

        setNotifications(notificationData);
      }
      return () => {
        sub = false;
      };
    });
  }, [userData?.uid, userData.badges]);

  const handleDelete = (uid, notificationId) => {
    deleteNotification(uid, notificationId);
    getAllNotificationByUserId(uid)
        .then((snapshot) => {
          const values = snapshot.exists() ? (Object.values(snapshot.val()).length) : 0;
          updateUserBadges(userData?.handle, values);
          const newData = {...userData, badges: notifications.length};
          setContext({
            user,
            userData: newData,
          });
        });
  };

  return (
    <div>
      {notifications.length ? <div>
        {notifications.map((notification) => {
          return (<div key={notification.notificationId} className="notificationBox"><br/><br/>
            {notification.notification} <br/><br/>
            {notification.date}
            <button className="btn btn-light" onClick={() => handleDelete(userData.uid, notification.notificationId)}>Delete</button>
            <br/><br/>
          </div>);
        })}
      </div> : <div className="notificationBox">No Notifications To Show!</div>}
    </div>
  );
};

export default Notifications;
