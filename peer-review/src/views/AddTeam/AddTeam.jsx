import './AddTeam.css';
import React, {useEffect} from 'react';
import {Form, Field, FormElement} from '@progress/kendo-react-form';
import {ref as storageRef, uploadBytes, getDownloadURL} from 'firebase/storage';
import {useNavigate} from 'react-router';
import {getAllUsers} from '../../services/users.services';
import {useState} from 'react';
import {useContext} from 'react';
import {AppContext} from '../../providers/AppContext';
import {MultiSelect} from '@progress/kendo-react-dropdowns';
import {addNewTeam, getAllTeams} from '../../services/teams.services';
import {descriptionValidator, nameValidator, ValidatedInput} from '../../common/validations';
import {KendoNotification} from '../KendoNotification.jsx/KendoNotifications';
import {storage} from '../../config/firebase-config';
import {createNotification} from '../../services/notifications.services';
import teamPic from '../../img/teamPic.png';
/**
 *
 * @return {*}
 */
export const AddNewTeam = () => {
  const navigate = useNavigate();
  const [users, setUsers] = useState([]);
  const [teamName, setTeamName] = useState([]);
  const [teamPictureUrl, setTeamPictureUrl] = useState([]);
  const {userData: {handle, uid}} = useContext(AppContext);
  useEffect(() => {
    getAllUsers()
        .then((response) => {
          const usersWithoutCurrent = response.filter((r) => r.handle !== handle);
          setUsers(usersWithoutCurrent);
        })

        .catch(console.error);
  }, []);

  // multiselector
  const [teamMembers, setTeamMembers] = React.useState([]);

  const onChange = (event) => {
    setTeamMembers([...event.value]);
  };
  // multiselector

  const uploadPicture = (e) => {
    e.preventDefault();
    const file = e.target[0]?.files?.[0];

    if (!file) {
      setState({type: 'error', message: 'Item not created!'});
      setTimeout(() => {
        setState({type: ''});
      }, 4000);
    };

    const fileValue = file?.name.split('.');
    const fileExtension = fileValue[fileValue.length - 1];

    if (fileExtension.toLowerCase() !== 'jpg' && fileExtension.toLowerCase() !== 'png' && fileExtension.toLowerCase() !== 'jfif' && fileExtension.toLowerCase() !== 'jpeg') {
      setState({type: 'error', message: 'The selected file should be of jpg/png/jfif/jpeg type!'});
      setTimeout(() => {
        setState({type: ''});
      }, 4000);
    }

    getAllTeams()
        .then((teams) => {
          if (teams.some((team) => team.teamName === teamName)) {
            setState({type: 'error', message: 'Such team already exists'});
            setTimeout(() => {
              setState({type: ''});
            }, 4000);
          };
        });


    const avatar = storageRef(storage, `teams/${teamName}/avatar`);

    uploadBytes(avatar, file)
        .then((snapshot) => {
          return getDownloadURL(snapshot.ref)
              .then((url) => {
                setTeamPictureUrl(url);
                setState({type: 'success', message: 'Success!'});
                setTimeout(() => {
                }, 1000);
              });
        })
        .catch(console.error);
  };

  // pop-up notification
  const [state, setState] = useState({
    type: '',
    message: '',
  });
  // pop-up notification

  const existingTeamName = (formTeamName) => {
    return getAllTeams()
        .then((teams) => {
          return teams.filter((team) => {
            if (team.teamName === formTeamName) {
              setState({type: 'error',
                message: 'Team name already exists!'});
              setTimeout(() => {
                setState({type: ''});
              }, 4000);

              return true;
            }
          });
        });
  };

  const handleChangeName = (e) => {
    setTeamName(e.value);
  };

  const handleSubmit = async (teamName) => {
    const matchingTeamName = await existingTeamName(teamName.user.name);

    if (matchingTeamName.length > 0) {
      return;
    }

    return (
      addNewTeam(teamMembers, handle, teamName.user.name, teamName.user.description, teamPictureUrl)
          .then((result) => {
            setState({type: 'success', message: 'Team added successfully!'});
            const data = {
              user: handle,
              userId: uid,
              type: 'Team',
              notification: `You have been invited to team ${teamName.user.name}`,
              teamId: result.id,
              date: new Date().toLocaleDateString(),
            };
            getAllUsers().then((users) => {
              users.map((user) => {
                if (teamMembers.includes(user.handle)) {
                  createNotification(user.uid, data);
                };
              });
            });
            setTimeout(function() {
              navigate('/project-lab');
            }, 1500);
          }).catch(console.error)
    );
  };

  return (
    <>
      <div className='task-container add-task-padding add-team'>
        <div className='form-container'>
          <KendoNotification
            type={state.type}
            message={state.message}>
          </KendoNotification>
          <Form
            onSubmit={handleSubmit}
            render={(formRenderProps) => (
              <FormElement
                style={{
                  maxWidth: 650,
                }}
              >
                <fieldset className={'k-form-fieldset'}>
                  <legend className={'k-form-legend'}>
                Please fill in the following information:
                  </legend>
                  {formRenderProps.visited &&
                formRenderProps.errors &&
                formRenderProps.errors.VALIDATION_SUMMARY && (
                    <div className={'k-messagebox k-messagebox-error'}>
                      {formRenderProps.errors.VALIDATION_SUMMARY}
                    </div>
                  )}
                  <div className='mb-3'>
                    <div>Team Name</div>
                    <Field
                      name={'user.name'}
                      component={ValidatedInput}
                      validator={nameValidator}
                      onChange = {handleChangeName}
                    />
                  </div>
                  <div className='mb-3'>
                    <div>Team Description</div>
                    <Field
                      name={'user.description'}
                      component={ValidatedInput}
                      validator={descriptionValidator}
                    />
                  </div>
                </fieldset>
                <div className="example-wrapper">
                  <div>
                    <div>Add users</div>
                    <MultiSelect data={users?.map((u) => u.handle)}
                      onChange={onChange} value={teamMembers} />
                  </div>
                </div>
                <div className='k-form-buttons'>
                  <button
                    type={'submit'}
                    className='k-button k-button-md k-rounded-md
                k-button-solid k-button-solid-base'
                    disabled={!formRenderProps.allowSubmit}
                  >
                Create New Team
                  </button>
                </div>
              </FormElement>
            )}
          />
          {teamName && teamName.length > 2 && teamName.length < 31 ? <form id="uploadForm" onSubmit={uploadPicture}>
            <input className="uploadInput" type="file" name="file"></input>
            <button id="submitFormBtn" className='k-button k-button-md k-rounded-md k-button-solid k-button-solid-base' type="submit">Upload</button>
          </form> : null}
        </div>
        <div className='image-container'>
          <img src={teamPic} alt="House Vector @transparentpng.com" style={{width: '400px', heigth: '400px'}}></img>
        </div>
      </div>
      <div className='wave wave-upper'></div>
      <div className='wave wave-lower'></div>
    </>
  );
};
