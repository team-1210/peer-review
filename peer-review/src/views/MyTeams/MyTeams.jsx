/* eslint-disable no-unused-vars */
import './MyTeams.css';
import {useNavigate} from 'react-router';
import React, {useContext, useEffect, useState, useId} from 'react';
import {AppContext} from '../../providers/AppContext';
import {getUserByHandle, getUserTeams} from '../../services/users.services';
import {
  Grid,
  GridColumn
  as Column} from '@progress/kendo-react-grid';
import {Window} from '@progress/kendo-react-dialogs';
import {updateUserTeamStatusById} from '../../services/teams.services';
import {AiOutlineTeam} from 'react-icons/ai';
import {orderBy} from '@progress/kendo-data-query';
import PropTypes from 'prop-types';


export const MyTeams = ({status, buttonsText}) => {
  MyTeams.propTypes = {
    status: PropTypes.string,
    buttonsText: PropTypes.array,
    dataItem: PropTypes.object,
    enterEdit: PropTypes.func,
  };

  const id = useId();
  const {userData: {handle}} = useContext(AppContext);
  const navigate = useNavigate();

  const [myTeams, setMyTeams] = useState([]);
  const [gridData, setGridData] = useState([]);
  const [visible, setVisible] = useState(false);
  const [teamDetails, setTeamDetails] = useState(true);

  const EDIT_FIELD = 'inEdit';

  // sort
  const [sort, setSort] = React.useState([]);
  const [allowUnsort, setAllowUnsort] = React.useState(true);
  const [multiple, setMultiple] = React.useState(false);

  const sortChange = (event) => {
    setGridData(getProducts(event.sort));
    setSort(event.sort);
  };

  const getProducts = (sort) => {
    return orderBy(gridData, sort);
  };
    // sort

  const toggleDialog = () => {
    setVisible(!visible);
  };

  useEffect(() => {
    getUserTeams(handle)
        .then((teams) => {
          const myTeams = teams.filter((invitationId) => invitationId.status === status);
          setMyTeams(myTeams);
          setGridData(myTeams);
        })
        .catch(console.error);
  }, []);

  const handleSearch = (searchTerm) => {
    setGridData(
        myTeams.filter((team) => {
          return (
            team.handle.toLowerCase().includes(searchTerm.toLowerCase()) ||
            team.teamName.toLowerCase().includes(searchTerm.toLowerCase())
          );
        }),
    );
  };

  const viewDetails = (event) => {
    event.teamMembers.map((member) => {
      getUserByHandle(member)
          .then((memberInfo) => {
            setTeamDetails(event);
          });
    });

    toggleDialog();
  };

  const creationDate = (props) => {
    const date = new Date(props.dataItem.createdOn).toString().substring(0, 21);
    <AiOutlineTeam />;
    return <td>{date}</td>;
  };

  const EditCommandCell = (props) => {
    return (
      <td>
        <button
          className='k-button k-button-md
          k-rounded-md k-button-solid k-button-solid-primary'
          onClick={() => props.enterEdit(props.dataItem)}
        >
          {buttonsText[0]}
        </button>
      </td>
    );
  };

  const changeTeamStatus = (event) => {
    const newData = gridData.filter((item) => {
      if (event.id === item.id) {
        event.status = 'Accepted';
        updateUserTeamStatusById(handle, event.id, 'Accepted') // filter
            .then((r) => console.log(r))
            .catch((e) => console.error(e));
        item = {...event};

        return false;
      }

      return item.id;
    });


    setGridData(newData);
  };

  const myEditCommandCell = (props) => {
    return (
     buttonsText[0] === 'View Details' ?
    <EditCommandCell {...props} enterEdit={viewDetails} />:
    <EditCommandCell {...props} enterEdit={changeTeamStatus} />
    );
  };

  return (
    <div className='datatable'>
      {myTeams.length > 0 ?
      <>
        <div className='datatableTitle'>
          <span className="k-spacer"></span>
          <span className="k-searchbox k-input k-input-md k-rounded-md k-input-solid k-grid-search">
            <span className="k-input-icon k-icon k-i-search"></span><input autoComplete='off'
              placeholder="Search by team name or creator..." title="Search..." onInput={(e) => handleSearch(e.target.value)} className="k-input-inner"/></span>
          <button className='k-button k-button-md k-rounded-md k-button-solid k-button-solid-base k-button-add-new' onClick={() => navigate('/add-new-team')}>Add New</button>
        </div>
        <Grid
          data={gridData}
          dataItemKey={'teamName'}
          sortable={{
            allowUnsort: allowUnsort,
            mode: multiple ? 'multiple' : 'single',
          }}
          sort={sort}
          onSortChange={sortChange}
          editField={EDIT_FIELD}>
          <Column field='teamName' title='Team Name' />
          <Column field='handle' title='Creator' />
          <Column field='createdOn' title='Created on' cell={creationDate} />
          <Column cell={myEditCommandCell} width='160px' title='Status'/>
        </Grid>
      </> : <div className='text-container'>
        <div>NO PENDING TEAMS YET!</div>
      </div>}
      {visible && (
        <Window title={'Status'} onClose={toggleDialog} initialHeight={350}>
          <div>Team Name: {teamDetails.teamName}</div>
          <div>Creator: {teamDetails.handle}</div>
          <div>Created On: {teamDetails.createdOn}</div>
          <div>Team Members:</div>
          {teamDetails.teamMembers?.map((member) => <div key={id}>{member}</div> )}<br/>
          <div>Team creation date:</div>
          {new Date(teamDetails.createdOn).toString().substring(0, 21)}
        </Window>
      )}
    </div>
  );
};
