import React, {useContext, useEffect, useState} from 'react';
import {useNavigate} from 'react-router';
import {AppContext} from '../../providers/AppContext';
import {addComment, getAllWorkItems, getCommentsByTask, updateWorkItemStatusById} from '../../services/workItems.service';
import {KendoNotification} from '../../views/KendoNotification.jsx/KendoNotifications';
import {Form, Field, FormElement} from '@progress/kendo-react-form';
import {FormRenderValidation} from '../../components/FormRenderValidation/FormRenderValidation';
import {SubmitButton} from '../../components/SubmitButton/SubmitButton';
import {FormTextArea} from '../../components/FormTextArea/FormTextArea';

export const ReviewTask = () => {
  const taskTitle = localStorage.getItem('task');
  const navigate = useNavigate();
  const {userData: {handle}} = useContext(AppContext);
  const [taskWithTeamId, setTaskWithTeamId] = useState([]);
  const [comments, setComments] = useState([]);

  // pop-up notification
  const [state, setState] = useState({
    type: '',
    message: 'test test',
  });
    // pop-up notification
  const task = taskWithTeamId[0];


  const handleSubmit = ({comments}) => {
    addComment(handle, taskWithTeamId[0].id, comments)
        .then(() => {
          setState({type: 'success', message: 'Success!'});
          setTimeout(function() {
            navigate('/home');
          }, 400);
        })
        .catch((err) => {
          console.log(err);
          setState({type: 'error', message: 'Comment is not created!'});
          setTimeout(() => {
            setState({type: ''});
          }, 4000);
        });
  };

  const changeWorkItemStatus = (workItemId, status, teamId) => {
    updateWorkItemStatusById(workItemId, status, teamId);
    navigate('review');
  };


  useEffect(() => {
    getAllWorkItems()
        .then((workItem) => {
          const filtered = workItem.filter((item) => item.title === taskTitle);
          setTaskWithTeamId(filtered);
        });
  }, []);

  useEffect(() => {
    getCommentsByTask(task?.id)
        .then((comments) => {
          setComments(comments);
        });
  }, []);

  return (
    <div>
      <KendoNotification
        type={state.type}
        message={state.message}
      ></KendoNotification>

      {task !== undefined ?
      <>
        <div>Task title: {task.title} <br /></div>
        <div className="team-photo-container">
          <span className="photo-blur">
            <img className="team-photo" src={task.attachment}></img>
          </span>
        </div>
        <div>Task Status: {task.itemStatus} <br /></div>
        <div>Reviewer: {task.reviewer} <br /></div>
        <div>Creator: {task.handle} <br /></div>
        <div>Task Description: {task.description} <br /></div>
        <button onClick={() => changeWorkItemStatus(taskWithTeamId[0].id, 'Accepted', task.teamId)}>Accept</button>
        <button onClick={() => changeWorkItemStatus(taskWithTeamId[0].id, 'Rejected', task.teamId)}>Reject</button>
      </> : null }


      <Form
        onSubmit={handleSubmit}
        render={(formRenderProps) => (
          <FormElement className='form-input-fields'>
            <fieldset className={'k-form-fieldset'}>
              <FormRenderValidation formRenderProps={formRenderProps} />
              <Field
                name='comments'
                component= {FormTextArea}
                label= 'Description'
              />
            </fieldset>
            <div className='example-wrapper'>
              <div></div>
            </div>
            <div className='k-form-buttons'>
              <SubmitButton text={'Submit'} formProps={formRenderProps} />
            </div>
          </FormElement>
        )}
      />

      {comments.length > 0 ? <div>{comments[0].commentBody}</div> : null}
    </div>
  );
};
