import './SingleTaskView.css';
import React, {useContext, useEffect, useState} from 'react';
import {AppContext} from '../../providers/AppContext';
import {
  addComment,
  getAllWorkItems,
  getCommentsByTask,
  updateWorkItemStatusById,
} from '../../services/workItems.service';
import {TextArea} from '@progress/kendo-react-inputs';
import {CommentView} from '../CommentView/CommentView';
import {KendoNotification} from '../KendoNotification.jsx/KendoNotifications';
import {createNotification} from '../../services/notifications.services';

export const SingleTaskView = () => {
  const taskTitle = localStorage.getItem('task');

  const {
    userData: {handle, uid},
  } = useContext(AppContext);
  const [taskWithTeamId, setTaskWithTeamId] = useState([]);
  const [taskComments, setTaskComments] = useState([]);
  const [text, setText] = useState();
  const [status, setStatus] = useState('');

  // pop-up notification
  const [state, setState] = useState({
    type: '',
    message: 'test test',
  });
  // pop-up notification

  const changeWorkItemStatus = (workItemId, status, teamId) => {
    updateWorkItemStatusById(workItemId, status, teamId);
  };

  useEffect(() => {
    getAllWorkItems().then((workItem) => {
      const filtered = workItem.filter((item) => item.title === taskTitle);
      getCommentsByTask(filtered[0].id).then((comments) => {
        setTaskComments(comments);
      });
      setTaskWithTeamId(filtered);
    });
  }, [status]);

  const handleSubmit = (comments) => {
    addComment(handle, taskWithTeamId[0].id, comments)
        .then((item) => {
          setTaskComments(taskComments.concat([item]));
          setState({type: 'success', message: 'Success!'});

          const data = {
            user: handle,
            userId: uid,
            type: 'Comment',
            notification: `You have received comment in regards to task ${taskWithTeamId[0].description} `,
            teamId: taskWithTeamId[0].teamId,
            date: new Date().toLocaleDateString(),
          };
          getAllUsers().then((users) => {
            const assignedTo = users.filter((user) => user.handle === taskWithTeamId[0].creator)[0];
            createNotification(assignedTo.uid, data);
          });
          setTimeout(function() {
          }, 400);
        })
        .catch((err) => {
          setState({type: 'error', message: 'Comment is not created!'});
          setTimeout(() => {
            setState({type: ''});
          }, 4000);
        });
  };

  const handleChange = (event) => {
    setText([event.value]);
  };

  const task = taskWithTeamId[0];
  const lastComment = taskComments.length - 1;

  return (
    <>
      <KendoNotification
        type={state.type}
        message={state.message}
      ></KendoNotification>
      <div className="task-container">
        {task !== undefined ? (
          <div>
            <div className="task-container">
              <div className="task task-details">
                <h1 className="main-task-title">{task.title}</h1>
                <p>{task.description}</p>
                <div className="task-items">
                  <div className="task-item">
                    <div>Reviewer: {task.reviewer} </div>
                  </div>
                  <div className="task-item">
                    <div>Task Status: {task.itemStatus} </div>
                  </div>
                  <div className="task-item">
                    <div>Creator: {task.creator}</div>
                  </div>
                  <div className="task-item">
                    <div>
                      Created On: {task.createdOn.toLocaleDateString('en-US')}
                    </div>
                  </div>
                </div>
              </div>
              <div className="task task-photo-container">
                <span className="photo-blur">
                  <img className="task-photo" src={task.attachment}></img>
                </span>
                <div></div>
                {task.reviewer === handle && task.itemStatus === 'Pending' ? (
              <div className='buttons-group'>
                <button
                  className='k-button k-button-md
                  k-rounded-md k-button-solid k-button-solid-primary'
                  onClick={() => {
                    changeWorkItemStatus(
                        taskWithTeamId[0].id,
                        'Under Review',
                        task.teamId,
                    );
                    setStatus('Under Review');
                  }}
                >
                  Start Review
                </button>
              </div>
            ) : null}
                {task.reviewer === handle && task.itemStatus === 'Under Review' || task.reviewer === handle && task.itemStatus === 'Change Requested'? (
              <div className='buttons-group'>
                <button
                  className='k-button k-button-md
                k-rounded-md k-button-solid k-button-solid-primary'
                  onClick={() => {
                    changeWorkItemStatus(
                        taskWithTeamId[0].id,
                        'Accepted',
                        task.teamId,
                    );
                    setStatus('Accepted');
                    setState({
                      type: 'success',
                      message: 'Task Accepted successfully!',
                    });
                    setTimeout(() => {
                      setState({type: ''});
                    }, 2000);
                  }}
                >
                  Accept
                </button>
                <button
                  className='k-button k-button-md
                k-rounded-md k-button-solid k-button-solid-primary'
                  onClick={() => {
                    if (taskComments[lastComment]?.handle === handle) {
                      changeWorkItemStatus(
                          taskWithTeamId[0].id,
                          'Rejected',
                          task.teamId,
                      );
                      setStatus('Rejected');
                      setState({
                        type: 'success',
                        message: 'Task Rejected successfully!',
                      });
                      setTimeout(() => {
                        setState({type: ''});
                      }, 2000);
                    } else {
                      setState({
                        type: 'error',
                        message: 'Please Add a comment first!',
                      });
                      setTimeout(() => {
                        setState({type: ''});
                      }, 2000);
                    }
                  }}
                >
                  Reject
                </button>
                <button
                  className='k-button k-button-md
                k-rounded-md k-button-solid k-button-solid-primary'
                  onClick={() => {
                    if (taskComments[lastComment]?.handle === handle) {
                      changeWorkItemStatus(
                          taskWithTeamId[0].id,
                          'Change Requested',
                          task.teamId,
                      );
                      setStatus('Change Requested');
                      setState({
                        type: 'success',
                        message: 'Change Requested successfully!',
                      });
                      setTimeout(() => {
                        setState({type: ''});
                      }, 2000);
                    } else {
                      setState({
                        type: 'error',
                        message: 'Please Add a comment first!',
                      });
                      setTimeout(() => {
                        setState({type: ''});
                      }, 2000);
                    }
                  }}
                >
                  Request Change
                </button>
              </div>
            ) : null}
              </div>
            </div>
            <div className="container comment-section">
              {taskComments.map((comment) => {
                return (
                  <CommentView key={comment.id} comment={comment}></CommentView>
                );
              })}
              <div className="comment-form">
                <TextArea
                  id={'1'}
                  onChange={handleChange}
                  maxLength={1000}
                  rows={4}
                />
                <button className='k-button k-button-md
                k-rounded-md k-button-solid k-button-solid-primary k-task-submit' type="button" onClick={() => handleSubmit(text)}>
                  Submit
                </button>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    </>
  );
};
