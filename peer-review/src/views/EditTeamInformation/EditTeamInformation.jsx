import React, {useEffect, useState} from 'react';
import {getTeamById} from '../../services/teams.services';
import {getAllUsers} from '../../services/users.services';
import './EditTeamInformation.css';

export const EditTeam = () => {
  const teamId = localStorage.getItem('teamId');
  const [teamInformation, setTeamInformation] = useState([]);
  const [tasksLength, setTasksLength] = useState(0);
  const [teamMembers, setTeamMembers] = useState([]);


  useEffect(() => {
    getTeamById(teamId)
        .then((team) => {
          setTeamInformation(team);
          if (team.workItems) {
            setTasksLength(Object.keys(team.workItems).length);
          }
          getAllUsers()
              .then((users) => {
                const teamUsers = users.filter((u) => team.teamMembers.includes(u.handle));
                setTeamMembers(teamUsers);
              });
        })
        .catch((e) => console.error(e));
  }, []);


  return (
    <div>
      <div className='task-container'>
        <div>
          <div className='task-container'>
            <div className='task task-details'>
              <h1 className='main-task-title'>{teamInformation.teamName}</h1>
              <p>{teamInformation.description}</p>
              <div className='task-items'>
                <div className='task-item'>
                  <div>Members: {teamMembers?.length} </div>
                </div>
                <div className='task-item'>
                  <div>Active tasks:{tasksLength} </div>
                </div>
                <div className='task-item'>
                  <div>Creator: {teamInformation.owner}</div>
                </div>
                <div className='task-item'>
                  <div>Created On: {teamInformation.createdOn?.toLocaleDateString('en-US')}</div>
                </div>
              </div>
            </div>
            <div className="task task-photo-container">
              <span className="photo-blur">
                <img className="task-photo" src={teamInformation.teamPicture}></img>
              </span>
            </div>
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-12 text-center">
          <div className="section-title mb-4 pb-2">
            <h1 className='main-task-title'>Our Creative Team</h1>
          </div>
        </div>
      </div>
      <div className="task-container">
        {teamMembers.map((member) => {
          return <div key={member.uid} className="container bootdey">

            <div className="row">
              <div className="col-md-9 col-12 mt-4 pt-2 img-wrapper">
                <img src={member.avatarUrl} className="rounded-circle" alt=""></img>
                <div className="team text-center rounded p-3 py-4">
                  <div className="content mt-3">
                    <h3 className="title mb-0">{member.firstName} {member.lastName}</h3>
                    <h5 className="text-muted">{member.handle}</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>;
        })}
      </div>
    </div>
  );
};
