import './Settings.css';
import React, {useContext} from 'react';
import {AppContext} from '../../providers/AppContext';
import {useNavigate} from 'react-router';
import defaultPicture from '../../img/249-2492113_work-profile-user-default-female-suit-comments-default.png';

/**
 * @return {*}
 */
export function Settings() {
  const {userData} = useContext(AppContext);
  const navigate = useNavigate();

  return (
    <div className="inf-content">
      <div className="signin-form">
        <div className="leftbar">
          {userData.avatarUrl ? <img src={userData.avatarUrl}
            className="user-uploaded-avatar" alt="user-uploaded-avatar" /> : <img className='uploaded-pic' src={defaultPicture} alt='defaultPicture' />}
          <div style={{textAlign: 'center'}}>
            <div style={{paddingTop: '1px'}}>
              <h2>{[userData.firstName, ' ', userData.lastName]}</h2>
            </div>
          </div>
        </div>
        <div className="signin-form2">
          <form className="form-cls">
            <div className='inf-content'>
              <div className="title-cls">
                <h4 className="edit-title">Edit your account</h4>
              </div>
              <div className="edit-cls-menu">
                <button className="k-button k-button-md k-rounded-md k-button-solid k-button-solid-base" onClick={() => navigate('/edit-account')}>
                  Edit Profile
                </button>
              </div>
              <div className="edit-cls-menu">
                <button className="k-button k-button-md k-rounded-md k-button-solid k-button-solid-base" onClick={() => navigate('/edit-password')}>
                Edit Password
                </button>
              </div>
              <div className="edit-cls-menu">
                <button className="k-button k-button-md k-rounded-md k-button-solid k-button-solid-base" onClick={() => navigate('/upload-picture')}>
                  Upload Pic
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
