import './Login.css';
import React, {useContext, useState} from 'react';
import {useNavigate} from 'react-router-dom';
import {AppContext} from '../../providers/AppContext';
import {loginUser} from '../../services/auth.service';
import {getUserData} from '../../services/users.services';
import {KendoNotification} from '../KendoNotification.jsx/KendoNotifications';
import taskPillar from '../../img/tasksPillar.png';

export const Login = () => {
  const navigate = useNavigate();
  const {setContext} = useContext(AppContext);

  const [form, setForm] = useState({
    email: '',
    password: '',
  });

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  // pop-up notification
  const [state, setState] = useState({
    type: '',
    message: '',
  });
  // pop-up notification

  const login = async (e) => {
    e.preventDefault();

    await loginUser(form.email, form.password)
        .then((u) => {
          getUserData(u.user.uid)
              .then((snapshot) => {
                if (snapshot.exists()) {
                  setContext({
                    user: u.user,
                    userData: snapshot.val()[Object.keys(snapshot.val())[0]],
                  });
                  navigate('/home');
                }
              });
        })
        .catch((err) => {
          setState({type: 'error', message: 'Wrong email or password!'});
          setTimeout(() => {
            setState({type: ''});
          }, 4000);
        });

    return false;
  };

  return (
    <>
      <div className='task-container add-task-padding add-team'>
        <div className='form-container'>
          <KendoNotification type={state.type}
            message={state.message}></KendoNotification>
          <div className='Login'>
            <div className='login-title-legend'>Fill the following:</div>
            <div className='Form'>
              <label htmlFor='email'>Email </label>
              <input className='login-input k-input k-input-md k-rounded-md k-input-solid' type="email" id="email" value={form.email}
                onChange={updateForm('email')}></input><br />
              <label htmlFor='password'>Password </label>
              <input className='login-input k-input k-input-md k-rounded-md k-input-solid' type="password" id="password" value={form.password}
                onChange={updateForm('password')}></input><br /><br />
              <div className='k-form-buttons'>
                <button className='k-button k-button-md k-rounded-md
                k-button-solid k-button-solid-base' onClick={login}>Login</button>
              </div>
            </div>
          </div>
        </div>
        <div className='image-container'>
          <img src={taskPillar} alt="House Vector @transparentpng.com" style={{width: '400px', heigth: '400px'}}></img>
        </div>
      </div>
      <div className='wave wave-upper'></div>
      <div className='wave wave-lower blue-background'></div>
      <div className='wave wave-upper blue-background'></div>
      <div className='wave wave-lower'></div>
    </>
  );
};
