import React from 'react';
import {
  Notification,
  NotificationGroup,
} from '@progress/kendo-react-notification';
import {Fade} from '@progress/kendo-react-animation';
import PropTypes from 'prop-types';


export const KendoNotification = ({type, message}) => {
  KendoNotification.propTypes = {
    type: PropTypes.string,
    message: PropTypes.string,
  };

  if (!type) {
    return;
  }

  return (
    <>
      <NotificationGroup
        style={{
          right: 0,
          bottom: 0,
          alignItems: 'flex-start',
          flexWrap: 'wrap-reverse',
        }}
      >
        <Fade>
          {type && (
            <Notification
              type={{style: type, icon: true}}
              closable={true}
            >
              <span>{message}</span>
            </Notification>
          )}
        </Fade>
      </NotificationGroup>
    </>
  );
};
