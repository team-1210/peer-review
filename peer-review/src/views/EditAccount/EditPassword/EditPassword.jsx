import React, {useContext, useState} from 'react';
import {useNavigate} from 'react-router';
import {AppContext} from '../../../providers/AppContext';
import {loginUser, updateUserPassword} from '../../../services/auth.service';
import {Form, Field, FormElement} from '@progress/kendo-react-form';
import {passwordValidator, ValidatedInput} from '../../../common/validations';
import {KendoNotification} from
  '../../KendoNotification.jsx/KendoNotifications';
import {SubmitButton} from '../../../components/SubmitButton/SubmitButton';
import {FormRenderValidation}
  from '../../../components/FormRenderValidation/FormRenderValidation';
import {FormTitle} from '../../../components/FormTitle/FormTitle';
import taskPillar from '../../../img/tasksPillar.png';

/**
 *
 * @return {*}
 */
export const EditPassword = () => {
  const {userData} = useContext(AppContext);
  const navigate = useNavigate();

  const [state, setState] = useState({
    type: '',
    message: '',
  });

  const editAccount = (form) => {
    loginUser(userData.email, form.user.password)
        .then((credentials) => {
          updateUserPassword(credentials.user, form.user.newPassword);
          setState({type: 'success', message: 'Success!'});
          setTimeout(function() {
            navigate('/home');
          }, 400);
        }) .catch((e) => {
          setState({type: 'error', message: 'Wrong password'});
          setTimeout(() => {
            setState({type: ''});
          }, 4000);
        });
  };

  return (
    <>
      <div className='task-container add-task-padding add-team'>
        <div className='form-container'>
          <KendoNotification
            type={state.type}
            message={state.message}>
          </KendoNotification>
          <Form
            onSubmit={editAccount}
            render={(formRenderProps) => (
              <FormElement
                className='form-input-fields'
              >
                <fieldset className={'k-form-fieldset'}>
                  <FormTitle title='Edit your password:'/>
                  <FormRenderValidation formRenderProps = {formRenderProps}/>
                  <div className='mb-3'>
                    <Field
                      name={'user.password'}
                      type={'password'}
                      component={ValidatedInput}
                      label={'Password'}
                      validator={passwordValidator}
                    />
                  </div>
                  <div className='mb-3'>
                    <Field
                      name={'user.newPassword'}
                      type={'password'}
                      component={ValidatedInput}
                      label={'New Password'}
                      validator={passwordValidator}
                    />
                  </div>
                </fieldset>
                <div className='k-form-buttons'>
                  <SubmitButton text={'Save'} formProps={formRenderProps}/>
                </div>
              </FormElement>
            )}
          />
        </div>
        <div className='image-container'>
          <img src={taskPillar} alt="House Vector @transparentpng.com" style={{width: '400px', heigth: '400px'}}></img>
        </div>
      </div>
      <div className='wave wave-upper'></div>
      <div className='wave wave-lower'></div>
    </>
  );
};
