import './UploadPicture.css';
import React, {useContext, useState} from 'react';
import {useNavigate} from 'react-router-dom';
import {AppContext} from '../../../providers/AppContext';
import {ref as storageRef, uploadBytes, getDownloadURL} from 'firebase/storage';
import {storage} from '../../../config/firebase-config';
import {updatePicture} from '../../../services/users.services';
import defaultPicture from '../../../img/249-2492113_work-profile-user-default-female-suit-comments-default.png';
import {KendoNotification} from '../../KendoNotification.jsx/KendoNotifications';


/**
 *
 * @return {JSX}
 */
export default function UploadPicture() {
  const {user, userData, setContext} = useContext(AppContext);
  const handle = userData.handle;
  const navigate = useNavigate();


  const [state, setState] = useState({
    type: '',
    message: '',
  });

  const uploadPicture = (e) => {
    e.preventDefault();

    const file = e.target[0]?.files?.[0];
    if (!file) {
      setState({type: 'error', message: 'Item not created!'});
      setTimeout(() => {
        setState({type: ''});
      }, 4000);
    };

    const fileValue = file?.name.split('.');
    const fileExtension = fileValue[fileValue.length - 1];

    if (fileExtension.toLowerCase() !== 'jpg' && fileExtension.toLowerCase() !== 'png' && fileExtension.toLowerCase() !== 'jfif' && fileExtension.toLowerCase() !== 'jpeg') {
      setState({type: 'error', message: 'The selected file should be of jpg/png/jfif/jpeg type!'});
      setTimeout(() => {
        setState({type: ''});
      }, 4000);
    }

    const picture = storageRef(storage, `images/${handle}/avatar`);

    uploadBytes(picture, file)
        .then((snapshot) => {
          return getDownloadURL(snapshot.ref)
              .then((url) => {
                return updatePicture(handle, url)
                    .then(() => {
                      setContext({
                        user,
                        userData: {
                          ...userData,
                          avatarUrl: url,
                        },
                      });
                      setState({type: 'success', message: 'Success!'});
                      setTimeout(() => {
                        navigate('/settings');
                      }, 1000);
                    });
              });
        })
        .catch((e) => {
          console.error(e.message);
        });
  };

  return (
    <>
      <KendoNotification type={state.type}
        message={state.message}></KendoNotification>
      <div className='inf-content'>
        <h2 className='upload-pic-title'>
          Upload or Change Your Profile Picture
        </h2>
        <br/>
        {
        userData.avatarUrl ?
        <>
          <img className='uploaded-pic' src={userData.avatarUrl} alt='profile' />
        </> : <img className='uploaded-pic' src={defaultPicture} alt='defaultPicture' /> }
        <form onSubmit={uploadPicture}>
          <input type='file' name='file'></input>
          <button type='submit' className='k-button k-button-md k-rounded-md k-button-solid submit-pic-button'>Upload</button>
        </form>
      </div>
    </>
  );
}
