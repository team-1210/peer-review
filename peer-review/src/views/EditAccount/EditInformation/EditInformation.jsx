import React, {useContext, useState} from 'react';
import {ref, update} from 'firebase/database';
import {useNavigate} from 'react-router';
import {db} from '../../../config/firebase-config';
import {AppContext} from '../../../providers/AppContext';
import {Form, Field, FormElement} from '@progress/kendo-react-form';
import {loginUser, updateUserEmail} from '../../../services/auth.service';
import {KendoNotification} from '../../KendoNotification.jsx/KendoNotifications';
import {fields} from '../../../common/user-field-types';
import {allInitialValues} from '../../../common/initial-values';
import {SubmitButton} from '../../../components/SubmitButton/SubmitButton';
import {FormRenderValidation} from '../../../components/FormRenderValidation/FormRenderValidation';
import {FormTitle} from '../../../components/FormTitle/FormTitle';
import taskPillar from '../../../img/tasksPillar.png';

/**
 *
 * @return {*}
 */
export default function EditInformation() {
  const {user, userData, setContext} = useContext(AppContext);

  const navigate = useNavigate();

  // pop-up notification
  const [state, setState] = useState({
    type: '',
    message: 'test test',
  });
  // pop-up notification


  const editUserAccount = (credentials, form) => {
    const updateAccount = {};
    const dbUser = form.user;
    const dbUsername = form.user.handle;
    const path = `users/${dbUsername}`;

     userData.firstName !== form.user.firstName ?
     updateAccount[`${path}/firstName`] = dbUser.firstName : null;

     userData.lastName !== form.user.lastName ?
     updateAccount[`${path}/lastName`] = dbUser.lastName : null;

     userData.phoneNumber !== form.user.phoneNumber ?
     updateAccount[`${path}/phoneNumber`] = dbUser.phoneNumber : null;

     userData.email !== form.user.email ?
     <>
       {updateUserEmail(credentials.user, dbUser.email)};
       {updateAccount[`users/${form.user.handle}/email`] = dbUser.email}
     </> : null;

     return update(ref(db), updateAccount);
  };


  const editAccount = (form) => {
    loginUser(userData.email, form.password)
        .then((credentials) => {
          editUserAccount(credentials, form);

          setContext({
            user,
            userData: {
              ...userData,
              firstName: form.user.firstName,
              lastName: form.user.lastName,
              phoneNumber: form.user.phoneNumber,
              email: form.user.email,
            },
          });

          setState({type: 'success', message: 'edit info'});
          setTimeout(function() {
            navigate('/home');
          }, 400);
        }).catch((err) => {
          setState({type: 'error', message: 'Wrong Password'});
          setTimeout(() => {
            setState({type: ''});
          }, 4000);
        });
  };

  return (
    <>
      <div className='task-container add-task-padding add-team'>
        <div className='form-container'>
          <KendoNotification
            type={state.type}
            message={state.message}>
          </KendoNotification>
          <Form
            initialValues={{
              user: allInitialValues(userData),
            }}
            onSubmit={editAccount}
            render={(formRenderProps) => (
              <FormElement
                className='form-input-fields'
              >
                <fieldset className={'k-form-fieldset'}>
                  <FormTitle title='Edit your information:'/>
                  <FormRenderValidation formRenderProps = {formRenderProps}/>
                  {fields.map((field) => {
                    return <div key={field.name} className='mb-3'>
                      <Field
                        name = {field.name}
                        component = {field.component}
                        label= {field.label}
                        type= {field.type}
                        validator = {field.validator}
                        readOnly={field.name === 'user.handle' ? true : false}
                      />
                    </div>;
                  })}
                </fieldset>
                <div className='k-form-buttons'>
                  <SubmitButton text={'Save'} formProps={formRenderProps}/>
                </div>
              </FormElement>
            )}
          />
        </div>
        <div className='image-container'>
          <img src={taskPillar} alt="House Vector @transparentpng.com" style={{width: '400px', heigth: '400px'}}></img>
        </div>
      </div>
      <div className='wave wave-upper'></div>
      <div className='wave wave-lower'></div>
    </>
  );
};
