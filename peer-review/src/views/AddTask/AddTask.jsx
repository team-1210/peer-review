import './AddTask.css';
import React, {useContext, useEffect} from 'react';
import {AppContext} from '../../providers/AppContext';
import {addWorkItem, getLiveWorkItems} from '../../services/workItems.service';
import CreateWorkItem from '../../components/CreateWorkItem/CreateWorkItem';
import taskBoardPic from '../../img/taskBoardPic.png';

export const AddTask = () => {
  const {userData: {handle, disabled}} = useContext(AppContext);

  useEffect(() => {
    const unsubscribe = getLiveWorkItems((snapshot) => {
    });

    return () => unsubscribe();
  }, []);

  const createWorkItem = (content) => {
    return addWorkItem(content, handle);
  };


  return (
    <>
      <div className='task-container add-task-padding'>
        <div className='form-container'>
          <div></div>
          {!disabled && <CreateWorkItem onSubmit={createWorkItem} />}
        </div>
        <div className='image-container'>
          <img src={taskBoardPic} alt="House Vector @transparentpng.com" style={{width: '400px', heigth: '400px'}}></img>
        </div>
      </div>
      <div className='wave wave-upper'></div>
      <div className='wave wave-lower'></div>
    </>
  );
};

