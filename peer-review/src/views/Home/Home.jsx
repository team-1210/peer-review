/* eslint-disable max-len */
import './Home.css';
import React, {useContext, useState, useEffect} from 'react';
import {mediaQueries} from '../../common/media-Queries';
import {birdDimensions} from '../../common/bird-dimensions.js';
import {birdConstants} from '../../common/bird-constants.js';
import {useLocation, useNavigate} from 'react-router-dom';
import {getClassName, getId} from '../../common/constants';
import {AppContext} from '../../providers/AppContext';

export const Home = () => {
  const {user} = useContext(AppContext);
  const [text, setText] = useState([]);
  const [buttons, setButtons] = useState([]);

  let currentMediaQuery = mediaQueries.find((item) => window.matchMedia(item).matches);
  const navigate = useNavigate();
  const location = useLocation();

  if (location.state?.from?.pathname) {
    navigate(location.state.from.pathname);
  }

  const onScroll = (event) => {
    const path = getClassName('left-path1')[0];
    const dimensions = birdDimensions.get(currentMediaQuery);
    const length = (window.scrollY >= 1600 ? (window.scrollY % 1600) : window.scrollY) / 4;
    const coords = path.getPointAtLength(length);
    const diffTop = coords.x;
    const diffLeft = coords.y;
    const currIndex = parseInt(window.scrollY / 1600);
    setText(birdConstants[currIndex]?.title);
    const cube = getId('bubble');
    cube.style = {};
    birdConstants[currIndex] ? cube.style['background-color'] = birdConstants[currIndex].color : null;
    cube.style[parseInt(window.scrollY / 1600) % 2 == 0 ? 'right': 'left'] = `${diffTop * dimensions.multiplierOffset.horizontal}px`;
    cube.style['top'] = `${diffLeft * dimensions.multiplierOffset.vertical}px`;

    for (let i = 0; i < birdConstants.length; i++) {
      birdConstants[i].partsToFill.map((el) => {
        getId(el).style.fill = birdConstants[i].color;
      });
    }

    for (let i = currIndex; i < birdConstants.length; i++) {
      birdConstants[i].partsToFill.map((el) => {
        getId(el).style.fill = '#E8E8E8';
      });
    }

    if (window.scrollY >= 7990) {
      setButtons(['Register', 'Login', 'Project Lab', 'My Board']);
      document.getElementById('bubble').style.display = 'none';
    } else {
      setButtons('');
    }
  };

  useEffect(() => {
    window.addEventListener('scroll', onScroll);
    return () => {
      window.removeEventListener('scroll', onScroll);
    };
  }, []);

  window.addEventListener('resize', function() {
    currentMediaQuery = mediaQueries.find((item) => window.matchMedia(item).matches);
  });

  return (
    <div className="loading-content">
      <div id="bubble"></div>
      <div id="message-box"><h1>{text}</h1></div>
      <div className="interior-design">
        <div className='innerSvg'>
          <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 480 480" style={{enableBackground: 'new 0 0 480 480', display: 'inline-block'}} xmlSpace="preserve">
            <g>
              <rect x="310.421" y="421.826" className = 'house' width="0" height="116.347"/>
              <path id='inner-roof' d="M240,53.718L58.512,247.306h362.975L240,53.718z M240,210.565c-20.292,0-36.741-16.45-36.741-36.741   c0-20.292,16.449-36.741,36.741-36.741c20.292,0,36.741,16.45,36.741,36.741C276.741,194.115,260.292,210.565,240,210.565z"/>
              <path id='inner-house' d="M56.351,249.612l-0.056,0.06v193.587h195.953V284.047h116.347v159.212h55.112V259.553H56.351   V249.612z M102.221,406.518v-73.482v-12.247v-24.494v-12.247h12.247h85.729h12.247v12.247v24.494v12.247v73.482v12.247h-12.247   h-85.729h-12.247V406.518z"/>
              <polygon id='line-under-roof' points="421.488,247.306 58.512,247.306 56.351,249.612 56.351,259.553 423.706,259.553    423.762,259.553 423.762,249.732  "/>
              <polygon id='roof' points="468.683,243.927 411.515,182.949 411.515,112.589 374.774,112.589 374.774,143.758 240,0    11.317,243.927 38.123,269.055 56.294,249.672 56.351,249.612 58.512,247.306 240,53.718 421.488,247.306 423.762,249.732    441.878,269.055  "/>
              <path id='window-sm' d="M240,137.083c-20.292,0-36.741,16.45-36.741,36.741c0,20.292,16.449,36.741,36.741,36.741   c20.292,0,36.741-16.45,36.741-36.741C276.741,153.532,260.292,137.083,240,137.083z M240,198.318   c-13.528,0-24.494-10.966-24.494-24.494c0-8.845,4.689-16.593,11.717-20.899c3.37-2.066,7.278-3.338,11.466-3.559   c0.434-0.023,0.872-0.036,1.312-0.036c13.528,0,24.494,10.966,24.494,24.494S253.528,198.318,240,198.318z"/>
              <path id='circle-down-window' d="M222.682,156.506c1.363-1.365,2.888-2.568,4.541-3.581c-7.027,4.306-11.717,12.054-11.717,20.899   c0,13.528,10.966,24.494,24.494,24.494c13.528,0,24.494-10.966,24.494-24.494c0,6.763-2.742,12.887-7.176,17.318L222.682,156.506z"/>
              <path id='circle-upper-window' d="M240,149.33c-0.44,0-0.877,0.013-1.312,0.036c-4.188,0.221-8.095,1.493-11.466,3.559   c-1.653,1.013-3.177,2.216-4.541,3.581l34.636,34.636c4.434-4.431,7.176-10.555,7.176-17.318   C264.494,160.297,253.527,149.33,240,149.33z"/>
              <polygon id='triangle-down-window' points="187.95,406.518 114.468,333.035 114.468,406.518 200.197,406.518  "/>
              <rect id='rectangle-window'x="114.468" y="296.294" width="85.729" height="24.494"/>
              <path id='outer-window' d="M200.197,418.765h12.247v-12.247v-73.482v-12.247v-24.494v-12.247h-12.247h-85.729h-12.247v12.247   v24.494v12.247v73.482v12.247h12.247H200.197z M114.468,296.294h85.729v24.494h-85.729V296.294z M114.468,333.035h85.729v73.482   h-85.729V333.035z"/>
              <polygon id='triangle-upper-window' points="200.197,406.518 200.197,333.035 114.468,333.035 187.95,406.518  "/>
              <polygon id='door' points="368.594,480 368.594,443.259 368.594,284.047 252.247,284.047 252.247,443.259 252.247,480     "/>
              <polygon id='bottom-right' points="252.247,443.259 56.294,443.259 56.294,480 252.247,480 252.247,480  "/>
              <polygon id='bottom-left' points="368.594,480 368.594,480 423.706,480 423.706,443.259 368.594,443.259  "/>
            </g>
          </svg>
        </div>
        <div className='innerSvg'>
          <svg className='car' xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 463.199 463.199" style={{enableBackground: 'new 0 0 463.199 463.199', width: '100px', display: 'inline-block'}} xmlSpace="preserve">
            <path id='car' d="M455.141,172.846  c-12.827-6.715-47.824,4.588-56.941,10.454c-2.985-5.311-6.372-11.128-10.044-17.084c-18.775-30.452-34.697-48.099-48.678-53.947  c-0.561-0.235-1.141-0.423-1.733-0.563c-1.199-0.283-30.256-6.925-106.144-6.925s-104.945,6.643-106.145,6.925  c-0.593,0.14-1.172,0.328-1.733,0.563c-13.98,5.849-29.903,23.495-48.678,53.947c-3.672,5.956-7.06,11.773-10.044,17.084  c-9.117-5.866-44.115-17.169-56.941-10.454c-13.235,6.929-7.932,28.738,0,34.653c4.952,3.692,18.539,4.816,32.847,3.865  c-3.237,4.311-5.788,9.163-7.515,14.389c22.564,4.23,75.605,4.047,67.678,21.244c-11.598,25.155-47.296,21.673-69.301,10.489  l4.109,70.924c0,16.574,13.436,30.009,30.01,30.009h18.047c16.574,0,30.009-13.435,30.009-30.009l-0.589-6.358h236.492l-0.589,6.358  c0,16.574,13.435,30.009,30.009,30.009h18.047c16.574,0,30.01-13.435,30.01-30.009l4.108-70.924  c-22.004,11.184-57.702,14.667-69.3-10.489c-7.928-17.197,45.113-17.014,67.678-21.244c-1.727-5.227-4.277-10.079-7.515-14.389  c14.308,0.952,27.895-0.172,32.847-3.865C463.072,201.584,468.376,179.775,455.141,172.846z M289.346,288.721H173.853l-12-43.755  h139.492L289.346,288.721z M85.872,191.233c14.366-25.134,32.724-51.975,45.553-58.161c5.379-1.057,34.726-6.151,100.175-6.151  c65.524,0,94.864,5.106,100.172,6.15c12.813,6.177,31.175,33.022,45.549,58.162H85.872z"/>
          </svg>
        </div>
      </div>
      <div className="loading-wrapper" style={{display: 'block'}}>
        <div style={{opacity: '1'}}></div>
        <svg className="bubbles_path_desktop" style={{opacity: '0'}} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 489.4 259.6" xmlSpace="preserve">
          <path className="left-path1" fill="none" stroke="#ccc" d="M5.4 243.5C29 133.3 127.1 50.6 244.4 50.6v58.6"></path>
          <path className="right-path1" fill="none" stroke="#ccc" d="M244.5 109.1V50.6c117.3 0 215.3 82.7 239 192.9"></path>
        </svg>
        <svg className="bubbles_path_mobile" style={{opacity: '0'}} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 489.4 259.6" xmlSpace="preserve">
          <path className="left-path" fill="none" stroke="#ccc" d="M2.1 205.1c42.5-91.2 135-154.4 242.3-154.4v64"></path>
          <path className="right-path" fill="none" stroke="#ccc" d="M244.5 114.5V50.6c107.2 0 199.7 63.3 242.2 154.4"></path>
        </svg>
      </div>
      {!user ?
      <>
        {buttons[0] ? <button id='register-home-page-button' className="interior-design home-page-button" onClick={() => navigate('/register')}>{buttons[0]}</button> : null}
        {buttons[1] ? <button id='login-home-page-button' className="interior-design home-page-button" onClick={() => navigate('/login')}>{buttons[1]}</button> : null}
      </> :
      <>
        {buttons[2] ? <button id='register-home-page-button' className="interior-design home-page-button" onClick={() => navigate('/project-lab')}>{buttons[2]}</button> : null}
        {buttons[3] ? <button id='login-home-page-button' className="interior-design home-page-button" onClick={() => navigate('/board-task')}>{buttons[3]}</button> : null}
      </>
      }
    </div>
  );
};
