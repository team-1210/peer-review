import {useNavigate} from 'react-router';
import React, {useEffect, useState} from 'react';
import {
  Grid,
  GridColumn
  as Column} from '@progress/kendo-react-grid';
import {getAllTeams} from '../../services/teams.services';
import {AiOutlineTeam} from 'react-icons/ai';
import PropTypes from 'prop-types';

export const AllTeams = () => {
  AllTeams.propTypes = {
    dataItem: PropTypes.object,
  };

  const navigate = useNavigate();

  const [myTeams, setMyTeams] = useState([]);
  const [gridData, setGridData] = useState([]);
  const EDIT_FIELD = 'inEdit';

  const setLocalStorage = (team, teamName) => {
    localStorage.setItem(team, teamName);
  };

  useEffect(() => {
    getAllTeams()
        .then((teams) => {
          setMyTeams(teams);
          setGridData(teams);
        })
        .catch(console.error);
  }, []);

  const handleSearch = (searchTerm) => {
    setGridData(
        myTeams.filter((team) => {
          return (
            team.handle.toLowerCase().includes(searchTerm.toLowerCase()) ||
            team.teamName.toLowerCase().includes(searchTerm.toLowerCase())
          );
        }),
    );
  };

  const creationDate = (props) => {
    const date = new Date(props.dataItem.createdOn).toString().substring(0, 21);
    <AiOutlineTeam />;
    return <td>{date}</td>;
  };

  const EditCommandCell = (props) => {
    return (
      <td>
        <button
          className='k-button k-button-md
          k-rounded-md k-button-solid k-button-solid-primary'
          onClick={() => {
            setLocalStorage('team', props.dataItem.teamName);
            setLocalStorage('teamId', props.dataItem.id);
            navigate(props.dataItem.teamName);
          }}
        >
          View Details
        </button>
      </td>
    );
  };

  return (
    <div className='datatable'>
      <div className='datatableTitle'>
        <span className="k-spacer"></span>
        <span className="k-searchbox k-input k-input-md k-rounded-md k-input-solid k-grid-search">
          <span className="k-input-icon k-icon k-i-search"></span><input autoComplete='off' placeholder="Search by team name or creator..."
            title="Search..." onInput={(e) => handleSearch(e.target.value)} className="k-input-inner"/></span>
        <button className='k-button k-button-md k-rounded-md k-button-solid k-button-solid-base k-button-add-new' onClick={() => navigate('/add-new-team')}>Add New</button>
      </div>

      {myTeams.length !== 0 ? (
        <Grid
          data={gridData}
          dataItemKey={'teamName'}
          editField={EDIT_FIELD}>
          <Column field='teamName' title='Team Name'/>
          <Column field='owner' title='Creator' />
          <Column field='createdOn' title='Created on' cell={creationDate} />
          <Column cell={EditCommandCell} width='160px' title='Status'/>
        </Grid>
      ) : null}
    </div>
  );
};
