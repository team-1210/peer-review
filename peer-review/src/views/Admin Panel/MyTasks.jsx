import React, {useContext, useEffect, useState} from 'react';
import {useNavigate} from 'react-router';
import {Grid, GridColumn as Column} from '@progress/kendo-react-grid';
import {groupBy} from '@progress/kendo-data-query';
import {setExpandedState, setGroupIds} from '@progress/kendo-react-data-tools';
import {getAllWorkItems} from '../../services/workItems.service';
import {AppContext} from '../../providers/AppContext';
import {getUserTeams} from '../../services/users.services';
import PropTypes from 'prop-types';

const initialGroup = [
  {
    field: 'creator',
  },
];

const processWithGroups = (data, group) => {
  const newDataState = groupBy(data, group);
  setGroupIds({
    data: newDataState,
    group: group,
  });
  return newDataState;
};

export const MyTasks = ({condition}) => {
  MyTasks.propTypes = {
    condition: PropTypes.string,
  };

  const {userData: {handle}} = useContext(AppContext);
  const [myTasks, setMyTasks] = useState([]);
  const [group, setGroup] = useState(initialGroup);
  const [resultState, setResultState] = React.useState(
      processWithGroups(myTasks, initialGroup),
  );

  // eslint-disable-next-line no-unused-vars
  const [collapsedState, setCollapsedState] = useState([]);
  const navigate = useNavigate();

  const setLocalStorage = (team, teamName) => {
    localStorage.setItem(team, teamName);
  };


  useEffect(() => {
    getUserTeams(handle)
        .then((teams) => {
          getAllWorkItems()
              .then((workItems) => {
                let myTasks = '';
                const myTeams = teams.filter((invitationId) => invitationId.status === 'Accepted');
                const onlyTeamNames = myTeams.map((team) => team.teamName);

                myTasks = condition === 'onlyMyTasks' ?
                  workItems.filter((workItem) => workItem.creator === handle || workItem.reviewer === handle):
                  workItems.filter((workItem) => onlyTeamNames.includes(workItem.team));
                setMyTasks(myTasks);
                setResultState(processWithGroups(myTasks, initialGroup));
              })
              .catch(console.error);
        });
  }, []);


  const onGroupChange =(event) => {
    const newDataState = processWithGroups(myTasks, event.group);
    setGroup(event.group);
    setResultState(newDataState);
  };

  const newData = setExpandedState({
    data: resultState,
    collapsedIds: collapsedState,
  });

  const viewTasks = ({dataItem, rowType}) => {
    if (rowType === 'groupHeader') {
      return null;
    }

    return (
      <td>
        <button
          className='k-button k-button-md
        k-rounded-md k-button-solid k-button-solid-primary'
          onClick={() => {
            setLocalStorage('team', dataItem.team);
            setLocalStorage('teamId', dataItem.teamId);
            setLocalStorage('task', dataItem.title);
            navigate(`/project-lab/tasks/overview/${dataItem.title}`);
          }}
        >
        View Details
        </button>
      </td>
    );
  };

  return (
    <div className='datatable'>
      <div className='datatableTitle'>
        <span className="k-spacer"></span>
        <span className="k-searchbox k-input k-input-md k-rounded-md k-input-solid k-grid-search">
          <span className="k-input-icon k-icon k-i-search"></span><input autoComplete='off' placeholder="Search by team name or creator..." title="Search..."
            onInput={(e) => handleSearch(e.target.value)} className="k-input-inner"/></span>
        <button className='k-button k-button-md k-rounded-md k-button-solid k-button-solid-base k-button-add-new' onClick={() => navigate('/add-new-task')}>Add New</button>
      </div>
      {newData.length !== 0 ? (
      <Grid
        style={{
          height: '520px',
        }}
        groupable={true}
        data={newData}
        onGroupChange={onGroupChange}
        group={group}
      >
        <Column field="title" title="Task Title"/>
        <Column field="creator" title="creator" />
        <Column field="reviewer" title="Reviewer" />
        <Column field="description" title="Description" />
        <Column field="itemStatus" title="Item Status"/>
        <Column cell={viewTasks} width='160px' title='View Task'/>
      </Grid>
      ) : null}
    </div>
  );
};
