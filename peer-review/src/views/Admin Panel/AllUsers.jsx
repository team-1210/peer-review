import './AdminPanel.css';
import React, {useEffect, useState} from 'react';
import {useNavigate} from 'react-router';
import PropTypes from 'prop-types';
import {CellRender, RowRender} from './renderers';
import {Grid, GridColumn as Column} from '@progress/kendo-react-grid';
import {editUserRole, editUserStatus, getAllUsers} from '../../services/users.services';
import {Window} from '@progress/kendo-react-dialogs';


/**
 *
 * @return {*}
 */
export const AllUsers = () => {
  const [users, setUsers] = useState([]);
  const [gridData, setGridData] = useState([]);
  const [visible, setVisible] = useState(false);
  const [teamDetails, setTeamDetails] = useState(true);
  const EDIT_FIELD = 'inEdit';
  const navigate = useNavigate();

  useEffect(() => {
    getAllUsers()
        .then((response) => {
          setUsers(response);
          setGridData(response);
        })

        .catch(console.error);
  }, []);

  const handleSearch = (searchTerm) => {
    setGridData(
        users.filter((item) => {
          return (
            item.email.includes(searchTerm) ||
          item.handle.includes(searchTerm) ||
          item.firstName.includes(searchTerm) ||
          item.lastName.includes(searchTerm)
          );
        }),
    );
  };

  const toggleUserStatus = (event) => {
    const newData = gridData.map((item) => {
      if (event.handle === item.handle) {
        event.disabled = !event.disabled;
        editUserStatus(event);
        item = {...event};
      }

      return item;
    });

    setGridData(newData);
  };

  const toggleDialog = () => {
    setVisible(!visible);
  };

  const viewDetails = (event) => {
    setTeamDetails(event);
    toggleDialog();
  };

  const enterEdit = (dataItem, field) => {
    const newData = gridData.map((item) => ({
      ...item,
      [EDIT_FIELD]: item.handle === dataItem.handle ? field : undefined,
    }));

    setGridData(newData);
    setUsers(newData);
  };

  const EditCommandCell = (props) => {
    return (
      <td>
        <button
          className='k-button k-button-md
          k-rounded-md k-button-solid k-button-solid-primary'
          onClick={() => props.enterEdit(props.dataItem)}
        >
          {props.dataItem.disabled ? 'Unblock' : 'Block'}
        </button>
      </td>
    );
  };

  const EditCommandCell1 = (props) => {
    EditCommandCell1.propTypes = {
      enterEdit: PropTypes.func,
      dataItem: PropTypes.string,
    };
    return (
      <td>
        <button
          className='k-button k-button-md
          k-rounded-md k-button-solid k-button-solid-primary'
          onClick={() => props.enterEdit(props.dataItem)}
        >
          View Details
        </button>
      </td>
    );
  };

  const MyEditCommandCell = (props) => (
    <EditCommandCell {...props} enterEdit={toggleUserStatus} />
  );

  const myEditCommandCell1 = (props) => {
    return (
      <EditCommandCell1 {...props} enterEdit={viewDetails} />
    );
  };

  const itemChange = (event) => {
    const field = event.field || '';
    const newData = gridData.map((item) =>{
      if (item.handle === event.dataItem.handle) {
        item[field] = event.value;
        editUserRole(item);
      }

      return item;
    });

    setGridData(newData);
  };

  EditCommandCell.propTypes = {
    dataItem: PropTypes.object,
    enterEdit: PropTypes.func,
  };

  const customCellRender = (td, props) => (
    <CellRender
      originalProps={props}
      td={td}
      enterEdit={enterEdit}
      editField={EDIT_FIELD}
    />
  );

  const exitEdit = () => {
    const newData = gridData.map((item) => ({...item,
      [EDIT_FIELD]: undefined}));
    setGridData(newData);
  };

  const createdOn = ({dataItem}) => {
    return new Date(dataItem.createdOn).toString().substring(3, 15);
  };

  const setLocalStorage = (team, teamName) => {
    localStorage.setItem(team, teamName);
  };

  const customRowRender = (tr, props) => (
    <RowRender
      originalProps={props}
      tr={tr}
      exitEdit={exitEdit}
      editField={EDIT_FIELD}
    />
  );

  return (
    <div className='datatable'>
      <div className='datatableTitle'>
        <span className="k-spacer"></span>
        <span className="k-searchbox k-input k-input-md k-rounded-md k-input-solid k-grid-search">
          <span className="k-input-icon k-icon k-i-search"></span>
          <input autoComplete='off' placeholder="Search by username, email, or name..." title="Search..." onInput={(e) => handleSearch(e.target.value)} className="k-input-inner"/>
        </span>
        <button className='k-button k-button-md k-rounded-md k-button-solid k-button-solid-base k-button-add-new' onClick={() => navigate('/add-new-team')}>Add New</button>
      </div>

      {users.length !== 0 ? (
        <Grid data={gridData}
          dataItemKey={'handle'}
          onItemChange={itemChange}
          cellRender={customCellRender}
          rowRender={customRowRender}
          editField={EDIT_FIELD}>
          <Column field='handle' title='Username' width='160px' editable={false} />
          <Column field='firstName' title='First Name' width='160px'
            editable={false} />
          <Column field='lastName' title='Last Name' width='160px'
            editable={false} />
          <Column field='phoneNumber' title='Phone Number' width='160px' editable={false} />
          <Column field='createdOn' title='Joined' width='160px' cell={createdOn}/>
          <Column field='role' title='Role' width='130px'/>
          <Column cell={MyEditCommandCell} title='Blocked'
            width='160px'/>
          <Column cell={myEditCommandCell1} width='160px' title='Status'/>

        </Grid>
      ) : null}

      {visible && (
        <Window title={'Status'} onClose={toggleDialog} initialHeight={550} initialWidth={450}>
          <h4 className='team-title'>All user Teams</h4>
          {Object.values(teamDetails.teams)?.map((team) => <div key={team.id} className='detailed-teams'>
            <h5>{team.teamName}</h5>
            <dl className=''>
              <dt className='label'>Team creator: </dt>
              <dd className='value'>{team.handle}</dd>
              <dd className=''></dd>
              <dt className='label'>Team created on: </dt>
              <dd className='value k-file-size'>{new Date(team.createdOn).toString().substring(0, 21)}</dd>
              <dd className=''></dd>
              <dt className='label'>User status in team:</dt>
              <dd className='value k-file-created'>{team.status}</dd>
              <dd className=''></dd>
            </dl>
            <div className='button-container'>
              <button
                className='k-button k-button-md
              k-rounded-md k-button-solid k-button-solid-primary'
                onClick={() => {
                  setLocalStorage('team', team.teamName);
                  navigate(team.teamName);
                }}
              >
              Visit Team
              </button>
            </div>
          </div> )}<br/>


        </Window>
      )}
    </div>
  );
};


