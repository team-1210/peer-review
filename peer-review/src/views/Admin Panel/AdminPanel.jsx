import React from 'react';
import {TabStrip, TabStripTab} from '@progress/kendo-react-layout';
import {MyTasks} from './MyTasks';
import {AllUsers} from './AllUsers';
import {AllTeams} from './AllTeams';

export const AdminPanel = () => { // da
  const [selected, setSelected] = React.useState(1);

  const handleSelect = (e) => {
    setSelected(e.selected);
  };

  return (
    <TabStrip selected={selected} onSelect={handleSelect}>
      <TabStripTab title="All Users">
        <div>
          <AllUsers />
        </div>
      </TabStripTab>
      <TabStripTab title="All Teams">
        <AllTeams />
      </TabStripTab>
      <TabStripTab title="All Tasks">
        <div>
          <MyTasks condition='allTasks'/>
        </div>
      </TabStripTab>
    </TabStrip>
  );
};
