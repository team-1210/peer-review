import './CommentView.css';
import React, {useContext, useEffect, useState} from 'react';
import {AppContext} from '../../providers/AppContext';
import {updateCommentLikes} from '../../services/workItems.service';
import {getAllUsers} from '../../services/users.services';
import PropTypes from 'prop-types';

export const CommentView = ({comment}) => {
  CommentView.propTypes = {
    comment: PropTypes.object,
  };

  const {userData} = useContext(AppContext);
  const [users, setUsers] = useState([]);
  const [commentLikeCount, setCommentLikes] = useState(comment.likes);
  useEffect(() => {
    getAllUsers()
        .then((users) => {
          return setUsers(users);
        });
  }, []);


  const commentLikes = async () => {
    const newLikeCount = await updateCommentLikes(comment, userData.handle);
    setCommentLikes(newLikeCount);
  };

  return (
    <div className="row">
      <div className="col-md-12">
        <div className="media g-mb-30 media-comment">
          {users.map((user) => {
            return user.handle === comment.handle ?
             <img key={user.uid} className="d-flex g-width-50 g-height-50 rounded-circle g-mt-3 g-mr-15" src={user.avatarUrl} alt="profile picture"></img> : null;
          })}
          <div className="media-body u-shadow-v18 g-bg-secondary g-pa-30">
            <div className="g-mb-15">
              <h5 className="h5 g-color-gray-dark-v1 mb-0">{comment.handle}</h5>
              <span className="g-color-gray-dark-v4 g-font-size-12">{new Date(comment.createdOn).toString().substring(0, 21)}</span>
            </div>
            <p>{comment.commentBody}</p>
            <ul className="list-inline d-sm-flex my-0">
              <li className="list-inline-item g-mr-20">
                <a className="u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#!" onClick={() => commentLikes()}>
                  <i className="fa fa-thumbs-up g-pos-rel g-top-1 g-mr-3"></i>
                  {commentLikeCount}
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};
