import './Projects.css';
import React from 'react';
import {TabStrip, TabStripTab} from '@progress/kendo-react-layout';
import {MyTeams} from '../MyTeams/MyTeams';


export const Projects = () => {
  const [selected, setSelected] = React.useState(1);
  const handleSelect = (e) => {
    setSelected(e.selected);
  };

  return (
    <TabStrip selected={selected} onSelect={handleSelect}>
      <TabStripTab icon='add' title="My Teams">
        <div>
          <MyTeams status={'Accepted'} buttonsText={['View Details', 'Leave']}/>
        </div>
      </TabStripTab>
      <TabStripTab title="Pending Team Invitations">
        <div>
          <MyTeams status={'Pending'} buttonsText={['Accept', 'Decline']}/>
        </div>
      </TabStripTab>
    </TabStrip>
  );
};
