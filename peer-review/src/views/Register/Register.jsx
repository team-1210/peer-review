import React from 'react';
import './Register.css';
import {Form, Field, FormElement} from '@progress/kendo-react-form';
import {useNavigate} from 'react-router';
import {registerUser} from '../../services/auth.service';
import {createUserHandle,
  getAllUsers,
  getUserByHandle} from '../../services/users.services';
import {useState} from 'react';
import {fields} from '../../common/user-field-types';
import {KendoNotification} from '../KendoNotification.jsx/KendoNotifications';
import {BiShowAlt} from 'react-icons/bi';
import {BiHide} from 'react-icons/bi';
import {Error} from '@progress/kendo-react-labels';
import {Input} from '@progress/kendo-react-inputs';
import {SubmitButton} from '../../components/SubmitButton/SubmitButton';
import {FormRenderValidation}
  from '../../components/FormRenderValidation/FormRenderValidation';
import {FormTitle} from '../../components/FormTitle/FormTitle';
import registerPic from '../../img/registerPic.png';


export const Register = () => {
  const navigate = useNavigate();
  const [passwordShown, setPasswordShown] = useState(false);

  // pop-up notification
  const [state, setState] = useState({
    type: '',
    message: '',
  });
  // pop-up notification

  const ValidatedIconInput = (fieldRenderProps) => {
    const {validationMessage, visited, ...others} = fieldRenderProps;
    return (
      <div>
        <Input {...others}/>
        <span className='custom-icon-class'>{passwordShown ? <BiShowAlt
          className='toggle-password-icon' onClick={togglePassword} /> :
              <BiHide className='toggle-password-icon'
                onClick={togglePassword}/>}</span>
        {visited && validationMessage && <Error>{validationMessage}</Error>}
      </div>
    );
  };

  const togglePassword = () => {
    setPasswordShown(!passwordShown);
  };


  const existingUsername = (handle) => {
    return getUserByHandle(handle)
        .then((snapshot) => {
          if (snapshot.exists()) {
            setState({type: 'error', message: 'Username already in use!'});
            setTimeout(() => {
              setState({type: ''});
            }, 4000);
            return true;
          }
        })
        .catch(console.error);
  };

  const existingNumber = (phoneNumber) => {
    return getAllUsers()
        .then((users) => {
          return users.filter((user) => {
            if (user.phoneNumber === phoneNumber) {
              setState({type: 'error',
                message: 'Phone Number Exists!'});
              setTimeout(() => {
                setState({type: ''});
              }, 4000);

              return true;
            }
          });
        });
  };

  const handleSubmit = async (form) => {
    const matchingPhoneNumbers = await existingNumber(form.user.phoneNumber);
    const matchingUsername = await existingUsername(form.user.handle);

    if (matchingPhoneNumbers.length > 0) {
      return;
    }

    if (matchingUsername) {
      return;
    }

    return (
      registerUser(form.user.email, form.password)
          .then((u) => {
            createUserHandle(form.user.handle, form.user.firstName,
                form.user.lastName, form.user.phoneNumber,
                u.user.uid, u.user.email)
                .then(() => {
                  setState({type: 'success', message: 'Success!'});
                  setTimeout(() => {
                    navigate('/home');
                  }, 1000);
                })
                .catch(console.error);
          })
          .catch((err) => {
            if (err.message.includes(`email-already-in-use`)) {
              setState({type: 'error', message: 'Email already in use!'});
              setTimeout(() => {
                setState({type: ''});
              }, 4000);
            }
          })
    );
  };

  return (
    <>
      <div className='task-container add-task-padding add-team'>
        <div className='form-container'>
          <KendoNotification
            type={state.type}
            message={state.message}>
          </KendoNotification>
          <Form
            onSubmit={handleSubmit}
            render={(formRenderProps) => (
              <FormElement
                className='form-input-fields'
              >
                <fieldset className={'k-form-fieldset'}>
                  <FormTitle title='Fill the following:'/>
                  <FormRenderValidation formRenderProps = {formRenderProps}/>
                  {fields.map((field) => {
                    return <div key={field.name} className='mb-3'>
                      <Field
                        name = {field.name}
                        component = {field.name === 'password' ?
                    ValidatedIconInput : field.component}
                        label= {field.label}
                        type= {field.type === 'password' &&
                    passwordShown ? '' : field.type}
                        validator = {field.validator}
                      />
                    </div>;
                  })}
                  <div className='mb-3'>
                  </div>
                </fieldset>
                <div className='k-form-buttons'>
                  <SubmitButton text={'Register'} formProps={formRenderProps}/>
                </div>
              </FormElement>
            )}
          />
        </div>
        <div className='image-container'>
          <img src={registerPic} alt="House Vector @transparentpng.com" style={{width: '400px', heigth: '400px'}}></img>
        </div>
      </div>
      <div className='wave wave-upper'></div>
      <div className='wave wave-lower blue-background'></div>
      <div className='wave wave-upper blue-background'></div>
      <div className='wave wave-lower'></div>
    </>
  );
};

