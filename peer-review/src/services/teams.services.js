import {ref, push, get, query, equalTo, orderByChild,
  update, onValue} from 'firebase/database';
import {db} from '../config/firebase-config';

export const inviteUserToTeam = (teamMembers, handle, teamName,
    createdOn, id, status, teamPicture) => {
  teamMembers.map((member) => {
    member === handle ? status = 'Accepted' : status;

    return update(ref(db, `/users/${member}/teams/${id}`),
        {teamMembers, handle, teamName, createdOn, id, status, teamPicture});
  });
};

export const updateUserTeamStatusById = (handle, id, status) => {
  return update(ref(db), {
    [`users/${handle}/teams/${id}/status`]: status,
  });
};


export const getTeamByIdAndHandles = (teamMembers, handle, teamName,
    createdOn, id, teamPicture) => {
  return get(ref(db, `teams/${id}`))
      .then((result) => {
        if (!result.exists()) {
          throw new Error(`Team with id ${id} does not exist!`);
        }

        const team = result.val();
        team.id = id;
        team.createdOn = new Date(team.createdOn);
        const status = 'Pending';
        teamMembers ? inviteUserToTeam(teamMembers, handle, teamName,
            createdOn, id, status, teamPicture) : null;

        return team;
      });
};

export const addNewTeam = (members, handle, teamName, description, teamPicture) => {
  const teamMembers = [...members, handle];
  const createdOn = Date.now();
  return push(
      ref(db, 'teams'),
      {
        teamMembers,
        teamPicture,
        description,
        teamName: teamName,
        owner: handle,
        createdOn: createdOn,
      },
  )
      .then((result) => {
        return getTeamByIdAndHandles(teamMembers, handle, teamName,
            createdOn, result.key, teamPicture);
      });
};


export const getTeamById = (id) => {
  return get(ref(db, `teams/${id}`))
      .then((result) => {
        if (!result.exists()) {
          throw new Error(`Team with id ${id} does not exist!`);
        }

        const team = result.val();
        team.id = id;
        team.createdOn = new Date(team.createdOn);

        return team;
      });
};


export const addUserToTeam = (teamMembers, teamInfo) => {
  const updateUsersTeam = {};
  teamMembers.map((handle) => {
    updateUsersTeam[`/users/${handle}/teams/`] = teamInfo;
  });

  return update(ref(db), updateUsersTeam);
};


export const fromTeamsDocument = (snapshot) => {
  const teamsDocument = snapshot.val();

  return Object.keys(teamsDocument).map((key) => {
    const team = teamsDocument[key];

    return {
      ...team,
      id: key,
      createdOn: new Date(team.createdOn),
    };
  });
};

export const getTeamsByTeamName = (teamName) => {
  return get(query(ref(db, 'teams'), orderByChild('teamName'),
      equalTo(teamName)))
      .then((snapshot) => {
        if (!snapshot.exists()) return [];

        return fromTeamsDocument(snapshot);
      });
};

export const getLiveTeams = (listen) => {
  return onValue(ref(db, 'teams'), listen);
};

export const getAllTeams = () => {
  return get(ref(db, 'teams'))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return [];
        }

        return fromTeamsDocument(snapshot);
      });
};

export const deleteTeam = async (id) => {
  return update(ref(db), {
    [`/teams/${id}`]: null,
  });
};

export const uploadTeamPicture = (teamName, url) => {
  return update(ref(db), {
    [`teams/${teamName}/avatarUrl`]: url,
  });
};
