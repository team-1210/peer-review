import {get, set, ref, query, equalTo, orderByChild,
  update} from 'firebase/database';
import {db} from '../config/firebase-config';

export const createTaskBoard = (team, id, title, description, status, priority, type) => {
  return set(ref(db, `taskBoard/${team}/${id}`), {team, id, title, description, status, priority, type,
    createdOn: Date.now()});
};

export const fromTaskBoardDocument = (snapshot) => {
  const taskDocument = snapshot.val();

  return Object.keys(taskDocument).map((key) => {
    const task = taskDocument[key];

    return {
      ...task,
      id: key,
      createdOn: new Date(task.createdOn),
    };
  });
};

export const getTaskBoardById = (id) => {
  return get(query(ref(db, 'taskBoard'), orderByChild('id'),
      equalTo(id)))
      .then((snapshot) => {
        if (!snapshot.exists()) return [];

        return fromTaskBoardDocument(snapshot);
      });
};


export const getAllTaskBoards = () => {
  return get(ref(db, 'taskBoard'))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return [];
        }

        return fromTaskBoardDocument(snapshot);
      });
};
export const getTaskBoardsByTeam = (teamName) => {
  return get(ref(db, `taskBoard/${teamName}`))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return [];
        }

        return fromTaskBoardDocument(snapshot);
      });
};
export const editTaskBoards = (team, id, title, description, status, priority, type) => {
  const updateAccount = {};
  const path = `taskBoard/${team}/${id}`;

  updateAccount[`${path}/title`] = title;
  updateAccount[`${path}/description`] = description;
  updateAccount[`${path}/status`] = status;
  updateAccount[`${path}/priority`] = priority;
  updateAccount[`${path}/type`] = type;


  return update(ref(db), updateAccount);
};


export const getAllColumns = () => {
  return get(ref(db, 'columns'))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return [];
        }

        return fromTaskBoardDocument(snapshot);
      });
};

export const createNewColumn = (id, title, status, edit) => {
  return set(ref(db, `columns/${id}`), {id, title, status, edit,
    createdOn: Date.now()});
};
