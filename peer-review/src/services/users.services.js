import {get, set, ref, query, equalTo, orderByChild,
  update} from 'firebase/database';
import {userRole} from '../common/user-role';
import {db} from '../config/firebase-config';

export const getUserByHandle = (handle) => {
  return get(ref(db, `users/${handle}`));
};

export const createUserHandle = (handle, firstName, lastName,
    phoneNumber, uid, email) => {
  return set(ref(db, `users/${handle}`), {handle, uid, email, firstName,
    phoneNumber, lastName, badges: 0, role: userRole.BASIC, createdOn: Date.now()});
};

export const getUserData = (uid) => {
  return get(query(ref(db, 'users'), orderByChild('uid'), equalTo(uid)));
};

export const updatePicture = (handle, url) => {
  return update(ref(db), {
    [`users/${handle}/avatarUrl`]: url,
  });
};

export const editUserStatus = (params) => {
  return update(ref(db), {
    [`users/${params.handle}/disabled`]: params.disabled,
  });
};

export const editUserRole = (params) => {
  return update(ref(db), {
    [`users/${params.handle}/role`]: params.role,
  });
};

const fromUsersDocument = (snapshot) => {
  const usersDocument = snapshot.val();
  return Object.keys(usersDocument).map((key) => {
    const user = usersDocument[key];

    return {
      ...user,
      id: key,
    };
  });
};


export const getAllUsers = () => {
  return get(ref(db, 'users'))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return [];
        }

        return fromUsersDocument(snapshot);
      });
};

export const getUserTeams = (handle) => {
  return get(ref(db, `users/${handle}/teams`))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return [];
        }

        return fromUsersDocument(snapshot);
      });
};

export const updateUserBadges = (handle, newBadges) => {
  const updateAvatar = {};

  updateAvatar[`/users/${handle}/badges`] = newBadges;

  return update(ref(db), updateAvatar);
};
