import {ref, push, set, get, update, onValue}
  from 'firebase/database';
import {db} from '../config/firebase-config';
import {getAllTeams} from './teams.services';

export const fromWorkItemsDocument = (snapshot) => {
  const workItemsDocument = snapshot.val();
  return Object.keys(workItemsDocument).map((key) => {
    const workItem = workItemsDocument[key];


    return {
      ...workItem,
      id: key,
      createdOn: new Date(workItem.createdOn),
      comments: workItem.comments ?
      Object.keys(workItem.comments) : [],
    };
  });
};

export const addWorkItem = (title, description, itemStatus, reviewer, team, handle, teamId, attachmentUrl) => {
  return push(
      ref(db, 'workItems'),
      {
        title,
        description,
        itemStatus,
        reviewer,
        team,
        teamId,
        comments: {},
        attachment: attachmentUrl,
        creator: handle,
        createdOn: Date.now(),
      },
  )
      .then((result) => {
        addTaskToTeam(result.key, title, description, itemStatus, reviewer, team, handle, teamId, attachmentUrl);
        return getWorkItemById(result.key);
      });
};

export const getWorkItemById = (itemId) => {
  return get(ref(db, `workItems/${itemId}`))
      .then((result) => {
        if (!result.exists()) {
          throw new Error(`Item with id ${itemId} does not exist!`);
        }

        const item = result.val();
        item.id = itemId;
        item.createdOn = new Date(item.createdOn);
        if (!item.comments) {
          item.comments = [];
        } else {
          item.comments = Object.keys(item.comments);
        }

        return item;
      });
};

export const addTaskToTeam = (itemId, title, description, itemStatus, reviewer, addedTeamName, handle, teamId, attachmentUrl, likes=0, dislikes=0) => {
  getAllTeams()
      .then((teams) => {
        teams.map((team) => {
          if (team.teamName === addedTeamName) {
            return update(ref(db, `/teams/${team.id}/workItems/${itemId}`),
                {title, description, itemStatus, reviewer, addedTeamName, handle, teamId, attachmentUrl, likes, dislikes});
          }
        });
      });
};

export const getLiveWorkItems = (listen) => {
  return onValue(ref(db, `workItems`), listen);
};

export const addComment = (handle, itemId, commentBody) => {
  const createdOn = Date.now();
  return push(
      ref(db, `workItems/${itemId}/comments`),
      {
        workItem: itemId,
        reviewer: handle,
        commentBody,
        createdOn,
        likes: 0,
      },
  )
      .then((result) => {
        getWorkItemById(result.key);
        return createComment(result.key, handle, itemId, commentBody, createdOn);
      });
};

export const createComment = (key, handle, itemId, commentBody, createdOn) => {
  const comment = {
    handle,
    itemId,
    key,
    commentBody,
    createdOn,
    likes: 0,
  };

  set(ref(db, `comments/${itemId}/${key}`), comment);

  return comment;
};


// export const addAttachment = (itemId, url) => {
//   return push(
//       ref(db, `workItems/${itemId}/attachments/${url}`),
//       {
//         workItem: itemId,
//         // attachmentUrl: url,
//       },
//   );
//   // .then((result) => {
//   //   return getWorkItemById(result.key);
//   // });
// };

// export const updateComments = (handle, itemId) => {
//   const updateComments = {};
//   updateComments[`/workItems/${itemId}/commentedBy/${handle}`] = true;
//   updateComments[`/users/${handle}/commentedItems/${itemId}`] = true;

//   return update(ref(db), updateComments);
// };

export const deleteWorkItem = async (itemId) => {
  const workItem = await getWorkItemById(itemId);
  const updateComments = {};
  workItem.comments.forEach((handle) => {
    updateComments[`/users/${handle}/comments/${itemId}`] = null;
  });

  await update(ref(db), updateComments);

  return update(ref(db), {
    [`/workItems/${itemId}`]: null,
    [`/teams/${workItem.teamId}/workItems/${itemId}`]: null,
  });
};

export const getAllWorkItems = () => {
  return get(ref(db, 'workItems'))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return [];
        }

        return fromWorkItemsDocument(snapshot);
      });
};

export const updateWorkItemStatusById = (workItemId, status, teamId) => {
  const updateWorkItemStatus = {};

  updateWorkItemStatus[`workItems/${workItemId}/itemStatus`] = status;
  updateWorkItemStatus[`/teams/${teamId}/workItems/${workItemId}/itemStatus`] = status;

  return update(ref(db), updateWorkItemStatus);
};

export const getCommentsByTask = (taskId) => {
  return get(ref(db, `comments/${taskId}`))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return [];
        }

        return fromWorkItemsDocument(snapshot);
      });
};


export const updateCommentLikes = (comment, handle) => {
  const updateCommentLikes = {};
  const userIncluded = comment.likedBy ? comment.likedBy[handle] : false;

  if (!userIncluded) {
    comment.likedBy = comment.likedBy || {};
    comment.likedBy[handle] = true;
    comment.likes += 1;
  } else {
    comment.likes -= 1;
    delete comment.likedBy[handle];
  }

  updateCommentLikes[`comments/${comment.itemId}/${comment.key}/likes`] = comment.likes;
  updateCommentLikes[`comments/${comment.itemId}/${comment.key}/likedBy/${handle}`] = userIncluded ? null : true;

  update(ref(db), updateCommentLikes);
  return comment.likes;
};

export const getAllTaskReactionsById = (itemId) => {
  return get(ref(db, `comments/${itemId}`))
      .then((result) => {
        if (!result.exists()) {
          throw new Error(`Task with id ${itemId} does not exist!`);
        }
        const reaction = result.val();

        return reaction;
      });
};
