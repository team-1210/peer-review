import {ref, get, push, update}
  from 'firebase/database';
import {db} from '../config/firebase-config';

export const createNotification = (userId, data) => {
  return push(ref(db, `notifications/${userId}`), data);
};

export const getAllNotificationByUserId = (userId) => {
  return get(ref(db, `notifications/${userId}`));
};

export const deleteNotification = (
    userId,
    notificationId,
) => {
  const deleteNotif = {};

  deleteNotif[`notifications/${userId}/${notificationId}`] = null;

  return update(ref(db), deleteNotif);
};
