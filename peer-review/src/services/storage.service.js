export const uploadPhoto = (e) => {
  e.preventDefault();

  const file = e.target[0]?.files?.[0];

  if (!file) return alert(`Please select a file!`);

  const picture = storageRef(storage, `images/${handle}/avatar`);

  uploadBytes(picture, file)
      .then((snapshot) => {
        return getDownloadURL(snapshot.ref)
            .then((url) => {
              return updatePicture(handle, url)
                  .then(() => {
                    setContext({
                      user,
                      userData: {
                        ...userData,
                        avatarUrl: url,
                      },
                    });
                    alert('Picture added successfully');
                  });
            });
      })
      .catch((e) => {
        console.error(e.message);
      });
};
