<img src="./images/saturn.png" alt="logo" width="100px" style="margin-top: 20px;"/>

# Peer Review

## 1. Project Description

The members of every organization are responsible for the quality of the products. One mechanism for self-regulation and coverage of the organization's standards is the peer work review system. This could be in many forms, but in general it is a peer review of the submitted work item - code, tests, documents etc. The workflow is usually the following: user submits some work (code, documentation), assigns a reviewer, the reviewer can comment, approve, request changes, or decline the work item. Once the reviewer approves the work item it is marked as complete.

The system should allow submitting work items, assigning another user as reviewer. The reviewer can add comments, request additional changes, approve, or decline the work item.

<br>

## 2. Project information

- Language and version: **JavaScript ES2020** and **React 18.1.0**// TODO
- Platform and version: **Node 16.14.2+**

<br>

## 3. Full project view

I. Public Part

The public part is accessible without authentication i.e., for anonymous users.

- Register : The public part provides register functionality.
- Login : Login form used to authenticate a user using username and password.

II. Private Part

Accessible only if the user is authenticated.

Registered users have private area accessible after successful login, where they could see:

- All review requests that they have created.
- All review request where they are assigned as reviewers.
- A list of the teams the user is part of.
- A list of pending team invitations (if no other notification method was implemented) // TODO
- Login.
- Logout.
- Update their profile.

III. Administrative part

Accessible to employees only. System administrators should have administrative access to the system and permissions to administer all major information objects in the system

- Administrators are able to create other administrators.
- Administrators are view all review requests and teams.
- Administrators are able to close (accept or reject) any review request.
- Administrators are able to add and remove any user to/from any team.
- Administrators are able to see all teams review requests.

<br>

## 4. Setup

1. Run `npm start`.
2. You are good to go.

**Important:** The page is going to be refreshed whenever changes are saved to React, JavaScript and CSS files. Sometimes updating the CSS might trigger an error in the hot reload script - if that happens just refresh the page in the browser. // TODO

<br>

## 5. Project structure

- `Dumb / Presentational Components` - their only responsibility is to present something to the DOM. //TODO
- `Smart / Container Component` - their responsibility is keep track of the state and care about how the app works.

- `images` - contains the images for styling the page
- `styles` - contains the CSS files included in the app
- `src` - where the app's JavaScript code lives, inside you can find
  - `common/constants.js` - `common` holds resources used by other files, such resources are the constants in `constants.js`. Take a look at the file - remember the rule about no magic strings and numbers, and no hardcoded values? It has some helpers methods including aliasing for `document.querySelector`.
  - `data` - holds the **data layer** of the app - we want to achieve separation of concerns. That means - hold the data in one place, the views in another, the event logic in a third place, etc. This separation allows us to replace each **layer** with another with little to no modification of the rest of the code. All of the data layer is already implemented for you. In this folder you can find:
    - `favorites.js` - the module responsible for adding and removing gifs from favorites. It is based on the browser feature `localStorage`. Even though the implementation is complete, you can research more about the `localStorage` and how it can be useful for storing an retrieving data
  - `events` - holds the core app logic. Event listeners in `index.js` use directly functions exposed in the `events` folder files. The role of those functions is to make a bridge between the data and the views, i.e. a function might have to retrieve all gifs matching a search condition, create a view rendering the gifs and displaying the created view. Inside the folder you can find:
    - `favorites-events.js` - its role is to react to the heart icon - switch the **favorite** status of a gif, i.e. add it to favorites or remove it.
    - `navigation-events.js` is related to navigation events such as clicking on nav links (TrendingGIFs, FavoriteGIFs, etc.), buttons to show more of a category or a gif. You will need to complete the implementation of its functions
    - `search-events.js` is related to the searchbox and the search functionality
    - `upload-events.js` is related to the uploading of GIFs
  - `requests-service.js` is the **service** file (services are another name for file providing some reusable functionality) providing access to the public API of the data layer. While in this template the data layer is inside the app, in the next exercise the data later will be completely moved to a server and the `request` service will be the place to access that data. You need to complete the implementation. Take a look at the `data/gifs.js` to find what you can use.
  - `views` is the folder containing the view **templates**. **Templates** are partial html files (containing not a whole page, but a small meaningful part of it) which can have placeholders for data as well. Example of a view: `<p>This article has ${article.viewCount} views.</p>`. You can explore the files in the folder - they contain functions mapping data to (and returning) template views:
    - `contact-view.js` - that will allow the user to see the contacts
    - `favorites-view.js` - that will allow the user to make a gif his favorite and display it somewhere in the app
    - `search-view.js`- that will allow the user to search gifs by a given query
    - `trending-gifs-view.js` - that will allow the user to see the top trending gifs
    - `upload-view.js`- that will allow the user to see his uploaded gifs

<br>

### 7. App logic flow //TODO

The core app logic is very simple

1. User triggers an event by clicking/tapping on a element (link, button, heart icon, etc.)
1. When an event is raised it is handled by an event listener in `index.js`
1. Depending on the event type and the event's target element (or its class, id, etc.) a specific function from the `events` folder is called, i.e. when a nav link is clicked the `loadPage` is called with the value of the `data-page` attribute of the clicked element
1. The appropriate event function from the `events` folder either:
    - updates the view from a template in `views`
    - retrieves data from the `request` service and updates the view from a template in `views` depending on that data (and the template)
1. The view is updated and the user can trigger an new event

You can follow the already implemented navigation event called on elements with the class `nav-link` and specifically how the Home page is loaded. The rest of the events and event handlers follow the same logic.

<br>

Explicit listing: // TODO

1. Templates/views:

    - `toContactView()`
    - `toFavoritesView(img, isActive)`
    - `toSearchView(isActive, img)`
    - `renderTrendingGifs()`
    - `detailedGifs()`
    - `userGif()`

1. Event functions:

    - `executeChangeColor()`
    - `toggleFavoriteStatus(e)`
    - `loadPage(page)`
    - `renderAll()`
    - `renderTrending()`
    - `renderFavorites()`
    - `renderUpload()`
    - `renderContact()`
    - `detailedGif()`

1. Request service functions:

    - `loadGifs(searchTerm)`
    - `postGifs(formData)`
    - `getFavorites()`
    - `getRandom()`
    - `loadTrendingGifs()`
    - `loadGifDetails(id)`
<br>